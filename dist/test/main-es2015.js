(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./$$_lazy_route_resource lazy recursive":
/*!******************************************************!*\
  !*** ./$$_lazy_route_resource lazy namespace object ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/app.component.html":
/*!**************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/app.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--\nAuthor: Subhanshu Chaddha\nContact: subhanshu.chaddha2@gmail.com\nCollege: AKGEC, Ghaziabad\n-->\n<router-outlet></router-outlet>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/components/admin/admin.component.html":
/*!*********************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/components/admin/admin.component.html ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--\nAuthor: Subhanshu Chaddha\nContact: subhanshu.chaddha2@gmail.com\nCollege: AKGEC, Ghaziabad\n-->\n<head>\n  <title>ADMIN</title>\n</head>\n<div class=\"admin_header\"><h2>WELCOME Admin</h2></div>\n<div class=\"admin\">\n  <form>\n    <input type=\"submit\" class=\"candidate\" value=\"Candidates\" (click)=\"candidates()\">\n  </form>\n  <form>\n    <input type=\"submit\" class=\"question\" value=\"Questions\"(click)=\"questions()\">\n  </form>\n</div>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/components/candidates/candidates.component.html":
/*!*******************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/components/candidates/candidates.component.html ***!
  \*******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--\nAuthor: Subhanshu Chaddha\nContact: subhanshu.chaddha2@gmail.com\nCollege: AKGEC, Ghaziabad\n-->\n<p>candidates works!</p>\n\n<div>\n  <table style=\"width:80%\" id=\"question\">\n    <tr>\n      <th>ID</th>\n      <th>NAME</th>\n      <th>ROLE</th>\n      <th>STUDENT_NO</th>\n      <th>EMAIL</th>\n      <th>BRANCH</th>\n      <th>PHONE_NUMBER</th>\n      <th>GENDER</th>\n      <th>HOSTELER</th>\n    </tr>\n    <tr *ngFor=\"let user of users\">\n      <td>{{user.id}}</td>\n      <td>{{user.name}}</td>\n      <td>{{user.role}}</td>\n      <td>{{user.student_number}}</td>\n      <td>{{user.email}}</td>\n      <td>{{user.branch}}</td>\n      <td>{{user.phone_number}}</td>\n      <td>{{user.gender}}</td>\n      <td>{{user.hosteler}}</td>\n    </tr>\n  </table>\n</div>\n\n<form>\n  <input type=\"submit\" class=\"createQuestion\" value=\"Create Candidate\" (click)=\"candidateForm()\">\n</form>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/components/confirmation-dialog/confirmation-dialog.component.html":
/*!*************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/components/confirmation-dialog/confirmation-dialog.component.html ***!
  \*************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--\nAuthor: Subhanshu Chaddha\nContact: subhanshu.chaddha2@gmail.com\nCollege: AKGEC, Ghaziabad\n-->\n<h3>Do you want to submit your responses ?</h3>\n<button mat-raised-button (click)=\"no()\">No</button>\n<button mat-raised-button (click)=\"yes()\">Yes</button>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/components/create-candidate/create-candidate.component.html":
/*!*******************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/components/create-candidate/create-candidate.component.html ***!
  \*******************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--\nAuthor: Subhanshu Chaddha\nContact: subhanshu.chaddha2@gmail.com\nCollege: AKGEC, Ghaziabad\n-->\n<p>create-candidate works!</p>\n\n<title>Login</title>\n<link href='https://fonts.googleapis.com/css?family=Kanit:200' rel='stylesheet'>\n<link href='https://fonts.googleapis.com/css?family=Sanchez|Karla' rel='stylesheet'>\n<link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css\">\n\n<div class=\"container\">\n  <div class=\"head\">\n    <h3>Computer Society of India</h3>\n  </div>\n  <div>\n    <mat-card style=\"width: 50vh\">\n      <form>\n        <mat-form-field >\n          <input matInput type=\"name\" class=\"inp\" [formControl]=\"controls.name\" placeholder=\"Name\" required/>\n        </mat-form-field>\n        <br>\n        <mat-form-field>\n          <input matInput type=\"tel\" class=\"inp\" [formControl]=\"controls.student_number\" placeholder=\"Student no.\"\n                 required/>\n        </mat-form-field>\n        <br>\n        <mat-form-field>\n          <input matInput type=\"email\" class=\"inp\" [formControl]=\"controls.email\" placeholder=\"Email\" required/>\n        </mat-form-field>\n        <br>\n        <mat-form-field>\n          <input matInput type=\"tel\" class=\"inp\" [formControl]=\"controls.phone_number\" placeholder=\"Mob.no.\" required/>\n        </mat-form-field>\n        <br>\n        <mat-form-field>\n          <mat-label>Branch</mat-label>\n          <mat-select [formControl]=\"controls.branch\">\n            <mat-option value=\"CS\">CS</mat-option>\n            <mat-option value=\"IT\">IT</mat-option>\n            <mat-option value=\"ECE\">ECE</mat-option>\n            <mat-option value=\"EN\">EN</mat-option>\n            <mat-option value=\"EI\">EI</mat-option>\n            <mat-option value=\"ME\">ME</mat-option>\n            <mat-option value=\"CE\">CE</mat-option>\n          </mat-select>\n        </mat-form-field>\n        <br>\n        <h3>Gender:-</h3>\n        <mat-radio-group aria-label=\"Select an mat-option\" [formControl]=\"controls.gender\">\n          <mat-radio-button name=\"gender\" value=\"male\">Male</mat-radio-button><br>\n          <mat-radio-button name=\"gender\" value=\"female\">Female</mat-radio-button><br>\n          <mat-radio-button name=\"gender\" value=\"other\">Other</mat-radio-button><br>\n        </mat-radio-group>\n        <br>\n        <h3>Hosteler</h3>\n        <mat-radio-group aria-label=\"Select an mat-option\" [formControl]=\"controls.hosteler\">\n          Yes\n          <mat-radio-button name=\"hosteler\" value=\"1\"></mat-radio-button>\n          No\n          <mat-radio-button name=\"hosteler\" value=\"0\"></mat-radio-button>\n        </mat-radio-group>\n        <br>\n        <button mat-raised-button (click)=\"createCandidate()\"> Submit</button>\n      </form>\n    </mat-card>\n  </div>\n</div>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/components/create-question/create-question.component.html":
/*!*****************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/components/create-question/create-question.component.html ***!
  \*****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--\nAuthor: Subhanshu Chaddha\nContact: subhanshu.chaddha2@gmail.com\nCollege: AKGEC, Ghaziabad\n-->\n<p xmlns=\"http://www.w3.org/1999/html\">create-question works!</p>\n\n<title>Create Question</title>\n<link href='https://fonts.googleapis.com/css?family=Kanit:200' rel='stylesheet'>\n<link href='https://fonts.googleapis.com/css?family=Sanchez|Karla' rel='stylesheet'>\n<link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css\">\n\n<div class=\"container\">\n\n  <form class=\"form\" [formGroup]=\"form\" (ngSubmit)=\"createQuestion()\">\n\n    <mat-select placeholder=\"Category\" [formControl]=\"controls.type\">\n      <mat-option value=\"html\">HTML</mat-option>\n      <mat-option value=\"css\">CSS</mat-option>\n      <mat-option value=\"js\">JS</mat-option>\n      <mat-option value=\"nodejs\">NODEJS</mat-option>\n<!--      <mat-option value=\"sql\">SQL</mat-option>-->\n<!--      <mat-option value=\"aptitude\">APTITUDE</mat-option>-->\n    </mat-select>\n    <hr>\n    <mat-form-field>\n      <textarea matInput class=\"inp\" [formControl]=\"controls.question\" placeholder=\"Question\"></textarea>\n    </mat-form-field>\n    <hr>\n    <div formArrayName=\"options\" *ngFor=\"let control of formData.controls, let i = index\">\n      <div [formGroupName]=\"i\">\n        <mat-radio-group formControlName=\"is_correct\">\n          <mat-radio-button [value]=true><input type=\"text\" class=\"inp\" formControlName=\"title\" placeholder=\"Title\"\n                                                required/></mat-radio-button>\n        </mat-radio-group>\n      </div>\n    </div>\n    <input class=\"submit inp\" type=\"submit\" value=\"Submit\">\n  </form>\n</div>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/components/login/login.component.html":
/*!*********************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/components/login/login.component.html ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--\nAuthor: Subhanshu Chaddha\nContact: subhanshu.chaddha2@gmail.com\nCollege: AKGEC, Ghaziabad\n-->\n<title>Login</title>\n\n<div class=\"container\">\n  <div class=\"head\">\n    <img src=\"../../../assets/images/CsiLogo.png\" alt=\"CSI\">\n    <h1>Computer Society of India</h1>\n  </div>\n\n  <form class=\"form\" action=\"\">\n    <label>\n      <input matInput type=\"email\" [formControl]=\"controls.email\" placeholder=\"Email\"/>\n    </label>\n    <hr>\n    <label>\n      <input type=\"password\" placeholder=\"Password\" [formControl]=\"controls.password\" matInput/>\n    </label>\n    <hr>\n    <input class=\"submit\" type=\"submit\" value=\"Login\" (click)=\"login()\">\n  </form>\n\n\n</div>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/components/questions/questions.component.html":
/*!*****************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/components/questions/questions.component.html ***!
  \*****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--\nAuthor: Subhanshu Chaddha\nContact: subhanshu.chaddha2@gmail.com\nCollege: AKGEC, Ghaziabad\n-->\n<p>questions works!</p>\n<div>\n  <table style=\"width:80%\" id=\"question\">\n    <tr>\n      <th>ID</th>\n      <th>TYPE</th>\n      <th>QUESTION</th>\n      <th>OPTIONS</th>\n    </tr>\n    <tr *ngFor=\"let question of questions\">\n      <td>{{question.id}}</td>\n      <td>{{question.type}}</td>\n      <td>{{question.question}}</td>\n      <td><span *ngFor=\"let option of question.options.data\">{{option.option}}, </span></td>\n    </tr>\n  </table>\n</div>\n\n<form>\n  <input type=\"submit\" class=\"createQuestion\" value=\"Create Question\" (click)=\"questionForm()\">\n</form>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/components/response/response.component.html":
/*!***************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/components/response/response.component.html ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--\nAuthor: Subhanshu Chaddha\nContact: subhanshu.chaddha2@gmail.com\nCollege: AKGEC, Ghaziabad\n-->\n<form class=\"resForm\">\n  <mat-form-field>\n    <textarea matInput placeholder=\"Share Your Reviews Regarding Workshop\" [formControl]=\"controls.input\"></textarea>\n  </mat-form-field>\n  <button class=\"resSub\" (click)=\"submitResponse()\">Submit</button>\n</form>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/components/rules/rules.component.html":
/*!*********************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/components/rules/rules.component.html ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--\nAuthor: Subhanshu Chaddha\nContact: subhanshu.chaddha2@gmail.com\nCollege: AKGEC, Ghaziabad\n-->\n<title>RULES</title>\n\n<div class=\"rules\">\n  <div class=\"rules_header\">\n    <img src=\"../../../assets/images/CsiLogo.png\" alt=\"hii\">\n    <h1>TEAM CSI</h1>\n  </div>\n  <h1 class=\"ruleHeading\">Rules</h1>\n  <div class=\"rulesSubHeading\">Please read carefully before starting the test:</div>\n  <p></p>\n  <ol>\n    <li> The test includes 30 questions</li>\n    <li> You will be given only 45 minutes to solve this paper.</li>\n    <li> The test comprises four sections - HTML, CSS, JavaScript, NodeJs with MongoDB.</li>\n    <li> All questions have only one correct option. You can change your answers before submitting the test.</li>\n    <li> You will be awarded 1 mark for each correct answer. There is No Negative Marking.</li>\n    <li> Once the question is marked, it cannot be left unanswered, but you can change your response before\n      submission.\n    </li>\n    <li> Do not reload the page in any condition else your data will be lost.</li>\n    <li> You can submit your test only once.</li>\n    <li>\n      <ul> You will be disqualified in the following conditions-\n        <li>If you try to close the browser.</li>\n        <li>If you try to open a new tab.</li>\n        <li>If you press the back button.</li>\n        <li>If you try to refresh the page.</li>\n        <li>If you indulge in any unfair means.</li>\n        <li>If you try to exit fullscreen.</li>\n      </ul>\n    </li>\n  </ol>\n\n  <!--  <div class=\"optionsSec\">-->\n  <!--    <h3>Please select 1 programming language below(compulsory)</h3>-->\n  <!--    <mat-radio-group class=\"check\" [formControl]=\"controls.type\">-->\n  <!--      <mat-radio-button color=\"warn\" name=\"type\" value=\"c\" checked> C</mat-radio-button>-->\n  <!--      <mat-radio-button color=\"warn\" name=\"type\" value=\"c++\"> C++</mat-radio-button>-->\n  <!--      <mat-radio-button color=\"warn\" name=\"type\" value=\"java\"> JAVA</mat-radio-button>-->\n  <!--    </mat-radio-group>-->\n  <!--  </div>-->\n  <div class=\"submit_button\">\n    <form><!--change the name of action to transfer to desired backend.-->\n      <input type=\"submit\" class=\"button\" value=\"START TEST\" (click)=\"startTest()\">\n    </form>\n  </div>\n\n</div>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/components/test/test.component.html":
/*!*******************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/components/test/test.component.html ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--\nAuthor: Subhanshu Chaddha\nContact: subhanshu.chaddha2@gmail.com\nCollege: AKGEC, Ghaziabad\n-->\n<link href='https://fonts.googleapis.com/css?family=Kanit:200' rel='stylesheet'>\n<link href='https://fonts.googleapis.com/css?family=Sanchez|Karla' rel='stylesheet'>\n<link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css\">\n\n<div class=\"container\">\n  <div class=\"heading\">\n    <img src=\"../../../assets/images/nodejs.png\" alt=\"\">\n    <h1>NODEJS WITH MONGODB</h1>\n    <img src=\"../../../assets/images/mongodb.png\" alt=\"\">\n  </div>\n  <div class=\"nav__container\">\n<!--    <div class=\"nav\" [class.bgBlack]=\"Arr[0]\" (click)=\"showHide(selectedType,0)\">-->\n<!--      <div id=\"heading_{{selectedType}}\">{{selectedType}}</div>-->\n<!--    </div>-->\n    <div class=\"nav\"  [class.bgBlack]=\"Arr[0]\" (click)=\"showHide('html',0)\">\n      <div id=\"heading_html\">HTML</div>\n    </div>\n    <div class=\"nav\" [class.bgBlack]=\"Arr[1]\" (click)=\"showHide('css',1)\">\n      <div id=\"heading_css\">CSS</div>\n    </div>\n    <div class=\"nav\" [class.bgBlack]=\"Arr[2]\" (click)=\"showHide('js', 2)\">\n      <div id=\"heading_js\">JavaScript</div>\n    </div>\n    <div class=\"nav\" [class.bgBlack]=\"Arr[3]\" (click)=\"showHide('nodejs', 3)\">\n      <div id=\"heading_nodejs\">NodeJs</div>\n    </div>\n  </div>\n\n  <div class=\"que\">\n\n    <div *ngFor=\"let type of types\">\n      <div id=\"{{type}}\">\n        <div class=\"questions\" *ngFor=\"let question of questions, let i = index\" >\n          <div *ngIf=\"question.type === type\">\n            <h3><pre id=\"{{i+1}}\">{{i+1}}. {{question.question}} ?</pre></h3>\n            <mat-radio-group (change)=\"onQuestionResponse(question.id, $event.value)\">\n              <div *ngFor=\"let option of question.options.data\">\n                <mat-radio-button color=\"primary\" [value]=\"option.id\" (change)=\"colorDiv(i+1)\">{{option.option}}</mat-radio-button>\n              </div>\n            </mat-radio-group>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n\n  <div class=\"nums\">\n    <div class=\"gd\" *ngFor=\"let dummy of ' '.repeat(30).split(''), let x = index\" id=\"ques{{x+1}}\">{{x + 1}}</div>\n  </div>\n\n  <div class=\"instruct\">\n    <ul>\n      <li><div class=\"grey\"></div>&nbsp;Grey Boxes represents unattempted questions</li>\n      <li><div class=\"green\"></div>&nbsp;Green boxes represent attempted questions</li>\n    </ul>\n  </div>\n  <div class=\"count_down\">TIME LEFT<div > <countdown #countdown id=\"time\" [config]=\"{leftTime: 2700}\">$!m!:$!s!</countdown></div></div>\n  <div class=\"submit\" (click)=\"submitTest()\">Submit</div>\n\n</div>\n"

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _components_login_login_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./components/login/login.component */ "./src/app/components/login/login.component.ts");
/* harmony import */ var _components_admin_admin_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./components/admin/admin.component */ "./src/app/components/admin/admin.component.ts");
/* harmony import */ var _components_rules_rules_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./components/rules/rules.component */ "./src/app/components/rules/rules.component.ts");
/* harmony import */ var _components_test_test_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./components/test/test.component */ "./src/app/components/test/test.component.ts");
/* harmony import */ var _components_create_candidate_create_candidate_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./components/create-candidate/create-candidate.component */ "./src/app/components/create-candidate/create-candidate.component.ts");
/* harmony import */ var _components_create_question_create_question_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./components/create-question/create-question.component */ "./src/app/components/create-question/create-question.component.ts");
/* harmony import */ var _components_candidates_candidates_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./components/candidates/candidates.component */ "./src/app/components/candidates/candidates.component.ts");
/* harmony import */ var _components_questions_questions_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./components/questions/questions.component */ "./src/app/components/questions/questions.component.ts");
/* harmony import */ var _components_response_response_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./components/response/response.component */ "./src/app/components/response/response.component.ts");
/* harmony import */ var _components_confirmation_dialog_confirmation_dialog_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./components/confirmation-dialog/confirmation-dialog.component */ "./src/app/components/confirmation-dialog/confirmation-dialog.component.ts");
/* harmony import */ var _components_can_deactivate_can_deactivate_guard__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./components/can-deactivate/can-deactivate.guard */ "./src/app/components/can-deactivate/can-deactivate.guard.ts");

/*
  Author: Subhanshu Chaddha
  Contact: subhanshu.chaddha2@gmail.com
  College: AKGEC, Ghaziabad
*/













const routes = [
    {
        path: '',
        component: _components_login_login_component__WEBPACK_IMPORTED_MODULE_3__["LoginComponent"]
    },
    {
        path: 'admin-home',
        component: _components_admin_admin_component__WEBPACK_IMPORTED_MODULE_4__["AdminComponent"]
    },
    {
        path: 'rules',
        component: _components_rules_rules_component__WEBPACK_IMPORTED_MODULE_5__["RulesComponent"],
    },
    {
        path: 'test',
        component: _components_test_test_component__WEBPACK_IMPORTED_MODULE_6__["TestComponent"],
        canDeactivate: [_components_can_deactivate_can_deactivate_guard__WEBPACK_IMPORTED_MODULE_13__["CanDeactivateGuard"]]
    },
    {
        path: 'candidates',
        component: _components_candidates_candidates_component__WEBPACK_IMPORTED_MODULE_9__["CandidatesComponent"]
    },
    {
        path: 'questions',
        component: _components_questions_questions_component__WEBPACK_IMPORTED_MODULE_10__["QuestionsComponent"]
    },
    {
        path: 'createCandidate',
        component: _components_create_candidate_create_candidate_component__WEBPACK_IMPORTED_MODULE_7__["CreateCandidateComponent"]
    },
    {
        path: 'createQuestion',
        component: _components_create_question_create_question_component__WEBPACK_IMPORTED_MODULE_8__["CreateQuestionComponent"]
    },
    {
        path: 'response',
        component: _components_response_response_component__WEBPACK_IMPORTED_MODULE_11__["ResponseComponent"]
    },
    {
        path: 'confirmation-dialog',
        component: _components_confirmation_dialog_confirmation_dialog_component__WEBPACK_IMPORTED_MODULE_12__["ConfirmationDialogComponent"]
    }
];
let AppRoutingModule = class AppRoutingModule {
};
AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], AppRoutingModule);



/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let AppComponent = class AppComponent {
    constructor() {
        this.title = 'test';
    }
};
AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-root',
        template: __webpack_require__(/*! raw-loader!./app.component.html */ "./node_modules/raw-loader/index.js!./src/app/app.component.html"),
        styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
    })
], AppComponent);



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _components_admin_admin_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./components/admin/admin.component */ "./src/app/components/admin/admin.component.ts");
/* harmony import */ var _components_login_login_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./components/login/login.component */ "./src/app/components/login/login.component.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _services_api_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./services/api.service */ "./src/app/services/api.service.ts");
/* harmony import */ var _services_user_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./services/user.service */ "./src/app/services/user.service.ts");
/* harmony import */ var _components_rules_rules_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./components/rules/rules.component */ "./src/app/components/rules/rules.component.ts");
/* harmony import */ var _components_test_test_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./components/test/test.component */ "./src/app/components/test/test.component.ts");
/* harmony import */ var _components_create_candidate_create_candidate_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./components/create-candidate/create-candidate.component */ "./src/app/components/create-candidate/create-candidate.component.ts");
/* harmony import */ var _components_create_question_create_question_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./components/create-question/create-question.component */ "./src/app/components/create-question/create-question.component.ts");
/* harmony import */ var _services_test_service__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./services/test.service */ "./src/app/services/test.service.ts");
/* harmony import */ var _components_candidates_candidates_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./components/candidates/candidates.component */ "./src/app/components/candidates/candidates.component.ts");
/* harmony import */ var _services_admin_service__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./services/admin.service */ "./src/app/services/admin.service.ts");
/* harmony import */ var _components_questions_questions_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./components/questions/questions.component */ "./src/app/components/questions/questions.component.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm2015/material.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm2015/animations.js");
/* harmony import */ var ngx_countdown_timer__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ngx-countdown-timer */ "./node_modules/ngx-countdown-timer/ngx-countdown-timer.umd.js");
/* harmony import */ var ngx_countdown_timer__WEBPACK_IMPORTED_MODULE_22___default = /*#__PURE__*/__webpack_require__.n(ngx_countdown_timer__WEBPACK_IMPORTED_MODULE_22__);
/* harmony import */ var ngx_countdown__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ngx-countdown */ "./node_modules/ngx-countdown/fesm2015/ngx-countdown.js");
/* harmony import */ var _components_response_response_component__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ./components/response/response.component */ "./src/app/components/response/response.component.ts");
/* harmony import */ var _components_confirmation_dialog_confirmation_dialog_component__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ./components/confirmation-dialog/confirmation-dialog.component */ "./src/app/components/confirmation-dialog/confirmation-dialog.component.ts");
/* harmony import */ var _components_can_deactivate_can_deactivate_guard__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! ./components/can-deactivate/can-deactivate.guard */ "./src/app/components/can-deactivate/can-deactivate.guard.ts");

/*
  Author: Subhanshu Chaddha
  Contact: subhanshu.chaddha2@gmail.com
  College: AKGEC, Ghaziabad
*/


























let AppModule = class AppModule {
};
AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
        declarations: [
            _app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"],
            _components_admin_admin_component__WEBPACK_IMPORTED_MODULE_6__["AdminComponent"],
            _components_login_login_component__WEBPACK_IMPORTED_MODULE_7__["LoginComponent"],
            _components_rules_rules_component__WEBPACK_IMPORTED_MODULE_12__["RulesComponent"],
            _components_test_test_component__WEBPACK_IMPORTED_MODULE_13__["TestComponent"],
            _components_create_candidate_create_candidate_component__WEBPACK_IMPORTED_MODULE_14__["CreateCandidateComponent"],
            _components_create_question_create_question_component__WEBPACK_IMPORTED_MODULE_15__["CreateQuestionComponent"],
            _components_candidates_candidates_component__WEBPACK_IMPORTED_MODULE_17__["CandidatesComponent"],
            _components_questions_questions_component__WEBPACK_IMPORTED_MODULE_19__["QuestionsComponent"],
            _components_response_response_component__WEBPACK_IMPORTED_MODULE_24__["ResponseComponent"],
            _components_confirmation_dialog_confirmation_dialog_component__WEBPACK_IMPORTED_MODULE_25__["ConfirmationDialogComponent"],
        ],
        imports: [
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
            _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_21__["BrowserAnimationsModule"],
            _app_routing_module__WEBPACK_IMPORTED_MODULE_4__["AppRoutingModule"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClientModule"],
            _angular_common__WEBPACK_IMPORTED_MODULE_8__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_9__["ReactiveFormsModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_9__["FormsModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_20__["MatDialogModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_20__["MatRadioModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_20__["MatSelectModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_20__["MatInputModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_20__["MatSnackBarModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_20__["MatFormFieldModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_20__["MatCardModule"],
            ngx_countdown_timer__WEBPACK_IMPORTED_MODULE_22__["CountdownTimerModule"],
            ngx_countdown__WEBPACK_IMPORTED_MODULE_23__["CountdownModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_20__["MatButtonModule"]
        ],
        providers: [
            _services_api_service__WEBPACK_IMPORTED_MODULE_10__["ApiService"],
            _services_user_service__WEBPACK_IMPORTED_MODULE_11__["UserService"],
            _services_test_service__WEBPACK_IMPORTED_MODULE_16__["TestService"],
            _services_admin_service__WEBPACK_IMPORTED_MODULE_18__["AdminService"],
            _components_can_deactivate_can_deactivate_guard__WEBPACK_IMPORTED_MODULE_26__["CanDeactivateGuard"]
        ],
        bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"]],
        schemas: [_angular_core__WEBPACK_IMPORTED_MODULE_2__["CUSTOM_ELEMENTS_SCHEMA"]]
    })
], AppModule);



/***/ }),

/***/ "./src/app/components/admin/admin.component.css":
/*!******************************************************!*\
  !*** ./src/app/components/admin/admin.component.css ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".admin_header {\n  text-align: center;\n}\n\n.admin {\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-orient: horizontal;\n  -webkit-box-direction: normal;\n          flex-direction: row;\n  -webkit-box-pack: justify;\n          justify-content: space-between;\n  padding: 0 30% 0 30%;\n  margin-top: 18%;\n}\n\n.candidate, .question {\n  background-color: #4CAF50;\n  border: 1px solid white;\n  color: white;\n  padding: 15px 32px;\n  text-align: center;\n  text-decoration: none;\n  display: inline-block;\n  font-size: 16px;\n  cursor: pointer;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9hZG1pbi9hZG1pbi5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0Usa0JBQWtCO0FBQ3BCOztBQUVBO0VBQ0Usb0JBQWE7RUFBYixhQUFhO0VBQ2IsOEJBQW1CO0VBQW5CLDZCQUFtQjtVQUFuQixtQkFBbUI7RUFDbkIseUJBQThCO1VBQTlCLDhCQUE4QjtFQUM5QixvQkFBb0I7RUFDcEIsZUFBZTtBQUNqQjs7QUFFQTtFQUNFLHlCQUF5QjtFQUN6Qix1QkFBdUI7RUFDdkIsWUFBWTtFQUNaLGtCQUFrQjtFQUNsQixrQkFBa0I7RUFDbEIscUJBQXFCO0VBQ3JCLHFCQUFxQjtFQUNyQixlQUFlO0VBQ2YsZUFBZTtBQUNqQiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvYWRtaW4vYWRtaW4uY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5hZG1pbl9oZWFkZXIge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbi5hZG1pbiB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgcGFkZGluZzogMCAzMCUgMCAzMCU7XG4gIG1hcmdpbi10b3A6IDE4JTtcbn1cblxuLmNhbmRpZGF0ZSwgLnF1ZXN0aW9uIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzRDQUY1MDtcbiAgYm9yZGVyOiAxcHggc29saWQgd2hpdGU7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgcGFkZGluZzogMTVweCAzMnB4O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICBmb250LXNpemU6IDE2cHg7XG4gIGN1cnNvcjogcG9pbnRlcjtcbn1cbiJdfQ== */"

/***/ }),

/***/ "./src/app/components/admin/admin.component.ts":
/*!*****************************************************!*\
  !*** ./src/app/components/admin/admin.component.ts ***!
  \*****************************************************/
/*! exports provided: AdminComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminComponent", function() { return AdminComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _services_admin_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/admin.service */ "./src/app/services/admin.service.ts");




let AdminComponent = class AdminComponent {
    constructor(adminService, router) {
        this.adminService = adminService;
        this.router = router;
    }
    ngOnInit() {
    }
    candidates() {
        this.adminService.candidates()
            .subscribe(res => {
            this.router.navigate(['/candidates']);
        });
    }
    questions() {
        this.router.navigate(['/questions']);
    }
};
AdminComponent.ctorParameters = () => [
    { type: _services_admin_service__WEBPACK_IMPORTED_MODULE_3__["AdminService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
];
AdminComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-admin',
        template: __webpack_require__(/*! raw-loader!./admin.component.html */ "./node_modules/raw-loader/index.js!./src/app/components/admin/admin.component.html"),
        styles: [__webpack_require__(/*! ./admin.component.css */ "./src/app/components/admin/admin.component.css")]
    })
], AdminComponent);



/***/ }),

/***/ "./src/app/components/can-deactivate/can-deactivate.guard.ts":
/*!*******************************************************************!*\
  !*** ./src/app/components/can-deactivate/can-deactivate.guard.ts ***!
  \*******************************************************************/
/*! exports provided: CanDeactivateGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CanDeactivateGuard", function() { return CanDeactivateGuard; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let CanDeactivateGuard = class CanDeactivateGuard {
    canDeactivate(component) {
        if (!component.canDeactivate()) {
            if (confirm('You have unsaved changes! If you leave, your changes will be lost.')) {
                return true;
            }
            else {
                return false;
            }
        }
        return true;
    }
};
CanDeactivateGuard = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()
], CanDeactivateGuard);



/***/ }),

/***/ "./src/app/components/can-deactivate/component-can-deactivate.ts":
/*!***********************************************************************!*\
  !*** ./src/app/components/can-deactivate/component-can-deactivate.ts ***!
  \***********************************************************************/
/*! exports provided: ComponentCanDeactivate */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ComponentCanDeactivate", function() { return ComponentCanDeactivate; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


class ComponentCanDeactivate {
    unloadNotification($event) {
        if (!this.canDeactivate()) {
            $event.returnValue = true;
        }
    }
}
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"])('window:beforeunload', ['$event'])
], ComponentCanDeactivate.prototype, "unloadNotification", null);


/***/ }),

/***/ "./src/app/components/candidates/candidates.component.css":
/*!****************************************************************!*\
  !*** ./src/app/components/candidates/candidates.component.css ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#question, th, td{\n  border: 1px solid black;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9jYW5kaWRhdGVzL2NhbmRpZGF0ZXMuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLHVCQUF1QjtBQUN6QiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvY2FuZGlkYXRlcy9jYW5kaWRhdGVzLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIjcXVlc3Rpb24sIHRoLCB0ZHtcbiAgYm9yZGVyOiAxcHggc29saWQgYmxhY2s7XG59XG4iXX0= */"

/***/ }),

/***/ "./src/app/components/candidates/candidates.component.ts":
/*!***************************************************************!*\
  !*** ./src/app/components/candidates/candidates.component.ts ***!
  \***************************************************************/
/*! exports provided: CandidatesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CandidatesComponent", function() { return CandidatesComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _services_admin_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/admin.service */ "./src/app/services/admin.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm2015/material.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _create_candidate_create_candidate_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../create-candidate/create-candidate.component */ "./src/app/components/create-candidate/create-candidate.component.ts");






let CandidatesComponent = class CandidatesComponent {
    constructor(adminService, dialog, router) {
        this.adminService = adminService;
        this.dialog = dialog;
        this.router = router;
        this.users = [];
    }
    ngOnInit() {
        this.adminService.candidates()
            .subscribe(res => {
            this.users = res;
        });
    }
    candidateForm() {
        const dialogref = this.dialog.open(_create_candidate_create_candidate_component__WEBPACK_IMPORTED_MODULE_5__["CreateCandidateComponent"]);
        dialogref.afterClosed().subscribe(() => {
            this.adminService.candidates()
                .subscribe(res => {
                this.users = res;
            });
        });
    }
};
CandidatesComponent.ctorParameters = () => [
    { type: _services_admin_service__WEBPACK_IMPORTED_MODULE_2__["AdminService"] },
    { type: _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatDialog"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] }
];
CandidatesComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-candidates',
        template: __webpack_require__(/*! raw-loader!./candidates.component.html */ "./node_modules/raw-loader/index.js!./src/app/components/candidates/candidates.component.html"),
        styles: [__webpack_require__(/*! ./candidates.component.css */ "./src/app/components/candidates/candidates.component.css")]
    })
], CandidatesComponent);



/***/ }),

/***/ "./src/app/components/confirmation-dialog/confirmation-dialog.component.css":
/*!**********************************************************************************!*\
  !*** ./src/app/components/confirmation-dialog/confirmation-dialog.component.css ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvY29uZmlybWF0aW9uLWRpYWxvZy9jb25maXJtYXRpb24tZGlhbG9nLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/components/confirmation-dialog/confirmation-dialog.component.ts":
/*!*********************************************************************************!*\
  !*** ./src/app/components/confirmation-dialog/confirmation-dialog.component.ts ***!
  \*********************************************************************************/
/*! exports provided: ConfirmationDialogComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConfirmationDialogComponent", function() { return ConfirmationDialogComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm2015/material.js");
/* harmony import */ var _services_test_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/test.service */ "./src/app/services/test.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");





let ConfirmationDialogComponent = class ConfirmationDialogComponent {
    constructor(testService, matDialog, router, dialogRef, data) {
        this.testService = testService;
        this.matDialog = matDialog;
        this.router = router;
        this.dialogRef = dialogRef;
        this.data = data;
        this.responses = {};
    }
    ngOnInit() {
        console.log(this.data);
    }
    yes() {
        console.log('answers', this.data);
        this.testService.createAnswers(this.data)
            .subscribe(res => {
            alert('Test Submitted Successfully');
            this.matDialog.closeAll();
            this.router.navigate(['/response']);
        }, error => {
            this.matDialog.closeAll();
            this.router.navigate(['/']);
            alert('Submission Failed');
        });
    }
    no() {
        this.dialogRef.close();
    }
};
ConfirmationDialogComponent.ctorParameters = () => [
    { type: _services_test_service__WEBPACK_IMPORTED_MODULE_3__["TestService"] },
    { type: _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialog"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialogRef"] },
    { type: undefined, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"], args: [_angular_material__WEBPACK_IMPORTED_MODULE_2__["MAT_DIALOG_DATA"],] }] }
];
ConfirmationDialogComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-confirmation-dialog',
        template: __webpack_require__(/*! raw-loader!./confirmation-dialog.component.html */ "./node_modules/raw-loader/index.js!./src/app/components/confirmation-dialog/confirmation-dialog.component.html"),
        styles: [__webpack_require__(/*! ./confirmation-dialog.component.css */ "./src/app/components/confirmation-dialog/confirmation-dialog.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](4, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MAT_DIALOG_DATA"]))
], ConfirmationDialogComponent);



/***/ }),

/***/ "./src/app/components/create-candidate/create-candidate.component.css":
/*!****************************************************************************!*\
  !*** ./src/app/components/create-candidate/create-candidate.component.css ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "*{\n  margin: 0;\n  padding: 0;\n  font-family: 'Kanit', sans-serif;\n}\n\n.container{\n  height: 90vh;\n  width: 80vh;\n  /*  background-color: aqua; */\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n          flex-direction: column;\n  -webkit-box-align: center;\n          align-items: center;\n  -webkit-box-pack: center;\n          justify-content: center;\n}\n\nh1{\n  margin-bottom: 5vh;\n  font-size: 50px;\n  color: #2A3579;\n}\n\nimg{\n  height: 80px;\n  margin-left: -75px;\n  margin-right: 5px;\n}\n\n.form{\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n          flex-direction: column;\n}\n\ninput{\n  margin: 5px;\n  border: 0px;\n  height: 6vh;\n  width: 25vw;\n  font-size: 20px;\n\n}\n\nmat-select{\n  margin: 5px;\n  border: 0px;\n  height: 6vh;\n  width: 25vw;\n  font-size: 20px;\n}\n\n.submit{\n  margin-top:3vh;\n  background-color: #2A3579;\n  color: white;\n  font-weight: 500;\n}\n\n.mat-radio-button ~ .mat-radio-button {\n  margin-left: 16px;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9jcmVhdGUtY2FuZGlkYXRlL2NyZWF0ZS1jYW5kaWRhdGUuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLFNBQVM7RUFDVCxVQUFVO0VBQ1YsZ0NBQWdDO0FBQ2xDOztBQUVBO0VBQ0UsWUFBWTtFQUNaLFdBQVc7RUFDWCw2QkFBNkI7RUFDN0Isb0JBQWE7RUFBYixhQUFhO0VBQ2IsNEJBQXNCO0VBQXRCLDZCQUFzQjtVQUF0QixzQkFBc0I7RUFDdEIseUJBQW1CO1VBQW5CLG1CQUFtQjtFQUNuQix3QkFBdUI7VUFBdkIsdUJBQXVCO0FBQ3pCOztBQUdBO0VBQ0Usa0JBQWtCO0VBQ2xCLGVBQWU7RUFDZixjQUFjO0FBQ2hCOztBQUNBO0VBQ0UsWUFBWTtFQUNaLGtCQUFrQjtFQUNsQixpQkFBaUI7QUFDbkI7O0FBQ0E7RUFDRSxvQkFBYTtFQUFiLGFBQWE7RUFDYiw0QkFBc0I7RUFBdEIsNkJBQXNCO1VBQXRCLHNCQUFzQjtBQUN4Qjs7QUFFQTtFQUNFLFdBQVc7RUFDWCxXQUFXO0VBQ1gsV0FBVztFQUNYLFdBQVc7RUFDWCxlQUFlOztBQUVqQjs7QUFDQTtFQUNFLFdBQVc7RUFDWCxXQUFXO0VBQ1gsV0FBVztFQUNYLFdBQVc7RUFDWCxlQUFlO0FBQ2pCOztBQUNBO0VBQ0UsY0FBYztFQUNkLHlCQUF5QjtFQUN6QixZQUFZO0VBQ1osZ0JBQWdCO0FBQ2xCOztBQUNBO0VBQ0UsaUJBQWlCO0FBQ25CIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy9jcmVhdGUtY2FuZGlkYXRlL2NyZWF0ZS1jYW5kaWRhdGUuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIip7XG4gIG1hcmdpbjogMDtcbiAgcGFkZGluZzogMDtcbiAgZm9udC1mYW1pbHk6ICdLYW5pdCcsIHNhbnMtc2VyaWY7XG59XG5cbi5jb250YWluZXJ7XG4gIGhlaWdodDogOTB2aDtcbiAgd2lkdGg6IDgwdmg7XG4gIC8qICBiYWNrZ3JvdW5kLWNvbG9yOiBhcXVhOyAqL1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbn1cblxuXG5oMXtcbiAgbWFyZ2luLWJvdHRvbTogNXZoO1xuICBmb250LXNpemU6IDUwcHg7XG4gIGNvbG9yOiAjMkEzNTc5O1xufVxuaW1ne1xuICBoZWlnaHQ6IDgwcHg7XG4gIG1hcmdpbi1sZWZ0OiAtNzVweDtcbiAgbWFyZ2luLXJpZ2h0OiA1cHg7XG59XG4uZm9ybXtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbn1cblxuaW5wdXR7XG4gIG1hcmdpbjogNXB4O1xuICBib3JkZXI6IDBweDtcbiAgaGVpZ2h0OiA2dmg7XG4gIHdpZHRoOiAyNXZ3O1xuICBmb250LXNpemU6IDIwcHg7XG5cbn1cbm1hdC1zZWxlY3R7XG4gIG1hcmdpbjogNXB4O1xuICBib3JkZXI6IDBweDtcbiAgaGVpZ2h0OiA2dmg7XG4gIHdpZHRoOiAyNXZ3O1xuICBmb250LXNpemU6IDIwcHg7XG59XG4uc3VibWl0e1xuICBtYXJnaW4tdG9wOjN2aDtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzJBMzU3OTtcbiAgY29sb3I6IHdoaXRlO1xuICBmb250LXdlaWdodDogNTAwO1xufVxuLm1hdC1yYWRpby1idXR0b24gfiAubWF0LXJhZGlvLWJ1dHRvbiB7XG4gIG1hcmdpbi1sZWZ0OiAxNnB4O1xufVxuIl19 */"

/***/ }),

/***/ "./src/app/components/create-candidate/create-candidate.component.ts":
/*!***************************************************************************!*\
  !*** ./src/app/components/create-candidate/create-candidate.component.ts ***!
  \***************************************************************************/
/*! exports provided: CreateCandidateComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreateCandidateComponent", function() { return CreateCandidateComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _services_admin_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/admin.service */ "./src/app/services/admin.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm2015/material.js");






let CreateCandidateComponent = class CreateCandidateComponent {
    constructor(adminService, dialogRef, router) {
        this.adminService = adminService;
        this.dialogRef = dialogRef;
        this.router = router;
        this.controls = {
            name: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            student_number: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            email: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].email, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            phone_number: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            branch: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            gender: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            hosteler: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
        };
        this.form = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"](this.controls);
    }
    ngOnInit() {
    }
    createCandidate() {
        if (this.form.invalid) {
            console.log('hii');
        }
        const data = this.form.value;
        this.adminService.createCandidate(data)
            .subscribe(res => {
            this.dialogRef.close();
            // this.router.(['/candidate']);
        });
    }
};
CreateCandidateComponent.ctorParameters = () => [
    { type: _services_admin_service__WEBPACK_IMPORTED_MODULE_3__["AdminService"] },
    { type: _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatDialogRef"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] }
];
CreateCandidateComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-create-candidate',
        template: __webpack_require__(/*! raw-loader!./create-candidate.component.html */ "./node_modules/raw-loader/index.js!./src/app/components/create-candidate/create-candidate.component.html"),
        styles: [__webpack_require__(/*! ./create-candidate.component.css */ "./src/app/components/create-candidate/create-candidate.component.css")]
    })
], CreateCandidateComponent);



/***/ }),

/***/ "./src/app/components/create-question/create-question.component.css":
/*!**************************************************************************!*\
  !*** ./src/app/components/create-question/create-question.component.css ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "*{\n  margin: 0;\n  padding: 0;\n  font-family: 'Kanit', sans-serif;\n}\n\n.container{\n  width: 100%;\n  /*  background-color: aqua; */\n  /*display: flex;*/\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n          flex-direction: column;\n  -webkit-box-align: center;\n          align-items: center;\n  -webkit-box-pack: center;\n          justify-content: center;\n}\n\nh1{\n  margin-bottom: 5vh;\n  font-size: 50px;\n  color: #2A3579;\n}\n\n.form{\n\n  display: -webkit-box;\n\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n          flex-direction: column;\n}\n\ninput{\n  margin: 5px;\n  border: 0px;\n  height: 6vh;\n  width: 25vw;\n  font-size: 20px;\n\n}\n\nmat-select{\n  margin: 5px;\n  border: 0px;\n  height: 6vh;\n  width: 25vw;\n  font-size: 20px;\n}\n\n.submit{\n  margin-top:3vh;\n  background-color: #2A3579;\n  color: white;\n  font-weight: 500;\n}\n\n.mat-radio-button ~ .mat-radio-button {\n  margin-left: 16px;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9jcmVhdGUtcXVlc3Rpb24vY3JlYXRlLXF1ZXN0aW9uLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxTQUFTO0VBQ1QsVUFBVTtFQUNWLGdDQUFnQztBQUNsQzs7QUFFQTtFQUNFLFdBQVc7RUFDWCw2QkFBNkI7RUFDN0IsaUJBQWlCO0VBQ2pCLDRCQUFzQjtFQUF0Qiw2QkFBc0I7VUFBdEIsc0JBQXNCO0VBQ3RCLHlCQUFtQjtVQUFuQixtQkFBbUI7RUFDbkIsd0JBQXVCO1VBQXZCLHVCQUF1QjtBQUN6Qjs7QUFHQTtFQUNFLGtCQUFrQjtFQUNsQixlQUFlO0VBQ2YsY0FBYztBQUNoQjs7QUFFQTs7RUFFRSxvQkFBYTs7RUFBYixhQUFhO0VBQ2IsNEJBQXNCO0VBQXRCLDZCQUFzQjtVQUF0QixzQkFBc0I7QUFDeEI7O0FBRUE7RUFDRSxXQUFXO0VBQ1gsV0FBVztFQUNYLFdBQVc7RUFDWCxXQUFXO0VBQ1gsZUFBZTs7QUFFakI7O0FBQ0E7RUFDRSxXQUFXO0VBQ1gsV0FBVztFQUNYLFdBQVc7RUFDWCxXQUFXO0VBQ1gsZUFBZTtBQUNqQjs7QUFDQTtFQUNFLGNBQWM7RUFDZCx5QkFBeUI7RUFDekIsWUFBWTtFQUNaLGdCQUFnQjtBQUNsQjs7QUFDQTtFQUNFLGlCQUFpQjtBQUNuQiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvY3JlYXRlLXF1ZXN0aW9uL2NyZWF0ZS1xdWVzdGlvbi5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiKntcbiAgbWFyZ2luOiAwO1xuICBwYWRkaW5nOiAwO1xuICBmb250LWZhbWlseTogJ0thbml0Jywgc2Fucy1zZXJpZjtcbn1cblxuLmNvbnRhaW5lcntcbiAgd2lkdGg6IDEwMCU7XG4gIC8qICBiYWNrZ3JvdW5kLWNvbG9yOiBhcXVhOyAqL1xuICAvKmRpc3BsYXk6IGZsZXg7Ki9cbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG59XG5cblxuaDF7XG4gIG1hcmdpbi1ib3R0b206IDV2aDtcbiAgZm9udC1zaXplOiA1MHB4O1xuICBjb2xvcjogIzJBMzU3OTtcbn1cblxuLmZvcm17XG5cbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbn1cblxuaW5wdXR7XG4gIG1hcmdpbjogNXB4O1xuICBib3JkZXI6IDBweDtcbiAgaGVpZ2h0OiA2dmg7XG4gIHdpZHRoOiAyNXZ3O1xuICBmb250LXNpemU6IDIwcHg7XG5cbn1cbm1hdC1zZWxlY3R7XG4gIG1hcmdpbjogNXB4O1xuICBib3JkZXI6IDBweDtcbiAgaGVpZ2h0OiA2dmg7XG4gIHdpZHRoOiAyNXZ3O1xuICBmb250LXNpemU6IDIwcHg7XG59XG4uc3VibWl0e1xuICBtYXJnaW4tdG9wOjN2aDtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzJBMzU3OTtcbiAgY29sb3I6IHdoaXRlO1xuICBmb250LXdlaWdodDogNTAwO1xufVxuLm1hdC1yYWRpby1idXR0b24gfiAubWF0LXJhZGlvLWJ1dHRvbiB7XG4gIG1hcmdpbi1sZWZ0OiAxNnB4O1xufVxuIl19 */"

/***/ }),

/***/ "./src/app/components/create-question/create-question.component.ts":
/*!*************************************************************************!*\
  !*** ./src/app/components/create-question/create-question.component.ts ***!
  \*************************************************************************/
/*! exports provided: CreateQuestionComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreateQuestionComponent", function() { return CreateQuestionComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _services_admin_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/admin.service */ "./src/app/services/admin.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm2015/material.js");





let CreateQuestionComponent = class CreateQuestionComponent {
    constructor(adminService, builder, dialogRef) {
        this.adminService = adminService;
        this.builder = builder;
        this.dialogRef = dialogRef;
        this.controls = {
            type: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            question: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            options: this.builder.array([
                this.createOption(),
                this.createOption(),
                this.createOption(),
                this.createOption(),
            ])
        };
        this.form = this.builder.group(this.controls);
    }
    ngOnInit() {
    }
    createOption() {
        return this.builder.group({
            title: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            is_correct: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](false, [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required])
        });
    }
    get formData() { return this.form.get('options'); }
    createQuestion() {
        if (this.form.invalid) {
            return;
        }
        const data = this.form.value;
        this.adminService.createQuestion(data)
            .subscribe(res => {
            this.dialogRef.close();
            // this.router.(['/candidate']);
        });
    }
};
CreateQuestionComponent.ctorParameters = () => [
    { type: _services_admin_service__WEBPACK_IMPORTED_MODULE_3__["AdminService"] },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
    { type: _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatDialogRef"] }
];
CreateQuestionComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-create-question',
        template: __webpack_require__(/*! raw-loader!./create-question.component.html */ "./node_modules/raw-loader/index.js!./src/app/components/create-question/create-question.component.html"),
        styles: [__webpack_require__(/*! ./create-question.component.css */ "./src/app/components/create-question/create-question.component.css")]
    })
], CreateQuestionComponent);



/***/ }),

/***/ "./src/app/components/form-can-deactivate/form-can-deactivate.ts":
/*!***********************************************************************!*\
  !*** ./src/app/components/form-can-deactivate/form-can-deactivate.ts ***!
  \***********************************************************************/
/*! exports provided: FormCanDeactivate */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FormCanDeactivate", function() { return FormCanDeactivate; });
/* harmony import */ var _can_deactivate_component_can_deactivate__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../can-deactivate/component-can-deactivate */ "./src/app/components/can-deactivate/component-can-deactivate.ts");

class FormCanDeactivate extends _can_deactivate_component_can_deactivate__WEBPACK_IMPORTED_MODULE_0__["ComponentCanDeactivate"] {
    canDeactivate() {
        return this.isValid;
    }
}


/***/ }),

/***/ "./src/app/components/login/login.component.css":
/*!******************************************************!*\
  !*** ./src/app/components/login/login.component.css ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "* {\n  margin: 0;\n  padding: 0;\n  font-family: 'Kanit', sans-serif;\n}\n\n.container {\n  height: 100vh;\n  width: 100%;\n  /*  background-color: aqua; */\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n          flex-direction: column;\n  -webkit-box-align: center;\n          align-items: center;\n  -webkit-box-pack: center;\n          justify-content: center;\n  color: rgb(255, 111, 104);\n}\n\n.head {\n  display: -webkit-box;\n  display: flex;\n\n}\n\nh1 {\n  margin-bottom: 5vh;\n  font-size: 50px;\n  color: #2A3579;\n}\n\nimg {\n  height: 80px;\n  -webkit-transform: translateY(-1.3rem);\n          transform: translateY(-1.3rem);\n  margin-left: -75px;\n  margin-right: 5px;\n}\n\n.form {\n\n  display: -webkit-box;\n\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n          flex-direction: column;\n}\n\ninput {\n  margin: 5px;\n  border: 0px;\n  height: 6vh;\n  width: 25vw;\n  font-size: 20px;\n\n}\n\n.submit {\n  margin-top: 3vh;\n  background-color: #2A3579;\n  color: white;\n  font-weight: 800;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9sb2dpbi9sb2dpbi5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsU0FBUztFQUNULFVBQVU7RUFDVixnQ0FBZ0M7QUFDbEM7O0FBRUE7RUFDRSxhQUFhO0VBQ2IsV0FBVztFQUNYLDZCQUE2QjtFQUM3QixvQkFBYTtFQUFiLGFBQWE7RUFDYiw0QkFBc0I7RUFBdEIsNkJBQXNCO1VBQXRCLHNCQUFzQjtFQUN0Qix5QkFBbUI7VUFBbkIsbUJBQW1CO0VBQ25CLHdCQUF1QjtVQUF2Qix1QkFBdUI7RUFDdkIseUJBQXlCO0FBQzNCOztBQUVBO0VBQ0Usb0JBQWE7RUFBYixhQUFhOztBQUVmOztBQUVBO0VBQ0Usa0JBQWtCO0VBQ2xCLGVBQWU7RUFDZixjQUFjO0FBQ2hCOztBQUVBO0VBQ0UsWUFBWTtFQUNaLHNDQUE4QjtVQUE5Qiw4QkFBOEI7RUFDOUIsa0JBQWtCO0VBQ2xCLGlCQUFpQjtBQUNuQjs7QUFFQTs7RUFFRSxvQkFBYTs7RUFBYixhQUFhO0VBQ2IsNEJBQXNCO0VBQXRCLDZCQUFzQjtVQUF0QixzQkFBc0I7QUFDeEI7O0FBRUE7RUFDRSxXQUFXO0VBQ1gsV0FBVztFQUNYLFdBQVc7RUFDWCxXQUFXO0VBQ1gsZUFBZTs7QUFFakI7O0FBRUE7RUFDRSxlQUFlO0VBQ2YseUJBQXlCO0VBQ3pCLFlBQVk7RUFDWixnQkFBZ0I7QUFDbEIiLCJmaWxlIjoic3JjL2FwcC9jb21wb25lbnRzL2xvZ2luL2xvZ2luLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIqIHtcbiAgbWFyZ2luOiAwO1xuICBwYWRkaW5nOiAwO1xuICBmb250LWZhbWlseTogJ0thbml0Jywgc2Fucy1zZXJpZjtcbn1cblxuLmNvbnRhaW5lciB7XG4gIGhlaWdodDogMTAwdmg7XG4gIHdpZHRoOiAxMDAlO1xuICAvKiAgYmFja2dyb3VuZC1jb2xvcjogYXF1YTsgKi9cbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGNvbG9yOiByZ2IoMjU1LCAxMTEsIDEwNCk7XG59XG5cbi5oZWFkIHtcbiAgZGlzcGxheTogZmxleDtcblxufVxuXG5oMSB7XG4gIG1hcmdpbi1ib3R0b206IDV2aDtcbiAgZm9udC1zaXplOiA1MHB4O1xuICBjb2xvcjogIzJBMzU3OTtcbn1cblxuaW1nIHtcbiAgaGVpZ2h0OiA4MHB4O1xuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVkoLTEuM3JlbSk7XG4gIG1hcmdpbi1sZWZ0OiAtNzVweDtcbiAgbWFyZ2luLXJpZ2h0OiA1cHg7XG59XG5cbi5mb3JtIHtcblxuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xufVxuXG5pbnB1dCB7XG4gIG1hcmdpbjogNXB4O1xuICBib3JkZXI6IDBweDtcbiAgaGVpZ2h0OiA2dmg7XG4gIHdpZHRoOiAyNXZ3O1xuICBmb250LXNpemU6IDIwcHg7XG5cbn1cblxuLnN1Ym1pdCB7XG4gIG1hcmdpbi10b3A6IDN2aDtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzJBMzU3OTtcbiAgY29sb3I6IHdoaXRlO1xuICBmb250LXdlaWdodDogODAwO1xufVxuIl19 */"

/***/ }),

/***/ "./src/app/components/login/login.component.ts":
/*!*****************************************************!*\
  !*** ./src/app/components/login/login.component.ts ***!
  \*****************************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _services_user_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/user.service */ "./src/app/services/user.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");





let LoginComponent = class LoginComponent {
    constructor(userService, router) {
        this.userService = userService;
        this.router = router;
        this.errorMessage = 'Incorrect Login Details';
        this.controls = {
            email: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].email, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            password: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required])
        };
        this.form = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"](this.controls);
    }
    ngOnInit() {
    }
    login() {
        if (this.form.invalid) {
            return;
        }
        const data = this.form.value;
        this.userService.login(data)
            .subscribe(res => {
            localStorage.setItem('auth_token', res.token);
            if (res.user.role === 'admin') {
                this.router.navigate(['/admin-home']);
            }
            else {
                this.router.navigate(['/rules']);
            }
        }, error1 => {
            alert(this.errorMessage);
        });
    }
};
LoginComponent.ctorParameters = () => [
    { type: _services_user_service__WEBPACK_IMPORTED_MODULE_3__["UserService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] }
];
LoginComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-login',
        template: __webpack_require__(/*! raw-loader!./login.component.html */ "./node_modules/raw-loader/index.js!./src/app/components/login/login.component.html"),
        styles: [__webpack_require__(/*! ./login.component.css */ "./src/app/components/login/login.component.css")]
    })
], LoginComponent);



/***/ }),

/***/ "./src/app/components/questions/questions.component.css":
/*!**************************************************************!*\
  !*** ./src/app/components/questions/questions.component.css ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#question, th, td{\n  border: 1px solid black;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9xdWVzdGlvbnMvcXVlc3Rpb25zLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSx1QkFBdUI7QUFDekIiLCJmaWxlIjoic3JjL2FwcC9jb21wb25lbnRzL3F1ZXN0aW9ucy9xdWVzdGlvbnMuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIiNxdWVzdGlvbiwgdGgsIHRke1xuICBib3JkZXI6IDFweCBzb2xpZCBibGFjaztcbn1cbiJdfQ== */"

/***/ }),

/***/ "./src/app/components/questions/questions.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/components/questions/questions.component.ts ***!
  \*************************************************************/
/*! exports provided: QuestionsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "QuestionsComponent", function() { return QuestionsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _services_admin_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/admin.service */ "./src/app/services/admin.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm2015/material.js");
/* harmony import */ var _create_question_create_question_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../create-question/create-question.component */ "./src/app/components/create-question/create-question.component.ts");





let QuestionsComponent = class QuestionsComponent {
    constructor(adminService, dialog) {
        this.adminService = adminService;
        this.dialog = dialog;
        this.questions = [];
    }
    ngOnInit() {
        this.adminService.questions()
            .subscribe(res => {
            this.questions = res;
        });
    }
    questionForm() {
        const dialogRef = this.dialog.open(_create_question_create_question_component__WEBPACK_IMPORTED_MODULE_4__["CreateQuestionComponent"]);
        dialogRef.afterClosed().subscribe(() => {
            this.adminService.questions()
                .subscribe(res => {
                this.questions = res;
            });
        });
    }
};
QuestionsComponent.ctorParameters = () => [
    { type: _services_admin_service__WEBPACK_IMPORTED_MODULE_2__["AdminService"] },
    { type: _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatDialog"] }
];
QuestionsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-questions',
        template: __webpack_require__(/*! raw-loader!./questions.component.html */ "./node_modules/raw-loader/index.js!./src/app/components/questions/questions.component.html"),
        styles: [__webpack_require__(/*! ./questions.component.css */ "./src/app/components/questions/questions.component.css")]
    })
], QuestionsComponent);



/***/ }),

/***/ "./src/app/components/response/response.component.css":
/*!************************************************************!*\
  !*** ./src/app/components/response/response.component.css ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "*{\n  margin: 0;\n  padding: 0;\n}\nform{\n  margin-top: 13rem;\n  margin-left: 50%;\n  -webkit-transform: translateX(-50%);\n          transform: translateX(-50%);\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n          flex-direction: column;\n  -webkit-box-align: center;\n          align-items: center;\n}\nmat-form-field{\n  width: 100%;\n height: 5rem;\n}\n.resSub{\n  width: 100%;\n  background: none;\n  background: #4CAF50;\n  color: #fff;\n  font-size: 2rem;\n  border: none;\n  padding: 1rem 1.5rem;\n  display: block;\n  margin-top: 6rem;\n  cursor: pointer;\n}\n\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9yZXNwb25zZS9yZXNwb25zZS5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsU0FBUztFQUNULFVBQVU7QUFDWjtBQUNBO0VBQ0UsaUJBQWlCO0VBQ2pCLGdCQUFnQjtFQUNoQixtQ0FBMkI7VUFBM0IsMkJBQTJCO0VBQzNCLG9CQUFhO0VBQWIsYUFBYTtFQUNiLDRCQUFzQjtFQUF0Qiw2QkFBc0I7VUFBdEIsc0JBQXNCO0VBQ3RCLHlCQUFtQjtVQUFuQixtQkFBbUI7QUFDckI7QUFDQTtFQUNFLFdBQVc7Q0FDWixZQUFZO0FBQ2I7QUFDQTtFQUNFLFdBQVc7RUFDWCxnQkFBZ0I7RUFDaEIsbUJBQW1CO0VBQ25CLFdBQVc7RUFDWCxlQUFlO0VBQ2YsWUFBWTtFQUNaLG9CQUFvQjtFQUNwQixjQUFjO0VBQ2QsZ0JBQWdCO0VBQ2hCLGVBQWU7QUFDakIiLCJmaWxlIjoic3JjL2FwcC9jb21wb25lbnRzL3Jlc3BvbnNlL3Jlc3BvbnNlLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIqe1xuICBtYXJnaW46IDA7XG4gIHBhZGRpbmc6IDA7XG59XG5mb3Jte1xuICBtYXJnaW4tdG9wOiAxM3JlbTtcbiAgbWFyZ2luLWxlZnQ6IDUwJTtcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGVYKC01MCUpO1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xufVxubWF0LWZvcm0tZmllbGR7XG4gIHdpZHRoOiAxMDAlO1xuIGhlaWdodDogNXJlbTtcbn1cbi5yZXNTdWJ7XG4gIHdpZHRoOiAxMDAlO1xuICBiYWNrZ3JvdW5kOiBub25lO1xuICBiYWNrZ3JvdW5kOiAjNENBRjUwO1xuICBjb2xvcjogI2ZmZjtcbiAgZm9udC1zaXplOiAycmVtO1xuICBib3JkZXI6IG5vbmU7XG4gIHBhZGRpbmc6IDFyZW0gMS41cmVtO1xuICBkaXNwbGF5OiBibG9jaztcbiAgbWFyZ2luLXRvcDogNnJlbTtcbiAgY3Vyc29yOiBwb2ludGVyO1xufVxuXG4iXX0= */"

/***/ }),

/***/ "./src/app/components/response/response.component.ts":
/*!***********************************************************!*\
  !*** ./src/app/components/response/response.component.ts ***!
  \***********************************************************/
/*! exports provided: ResponseComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ResponseComponent", function() { return ResponseComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _services_test_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/test.service */ "./src/app/services/test.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");





let ResponseComponent = class ResponseComponent {
    constructor(testService, router) {
        this.testService = testService;
        this.router = router;
        this.controls = {
            input: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required)
        };
        this.form = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]((this.controls));
    }
    ngOnInit() {
        this.testService.createScore()
            .subscribe();
    }
    submitResponse() {
        const data = this.form.value;
        this.testService.responses(data)
            .subscribe(() => {
            this.router.navigate(['/']);
        });
    }
};
ResponseComponent.ctorParameters = () => [
    { type: _services_test_service__WEBPACK_IMPORTED_MODULE_3__["TestService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] }
];
ResponseComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-response',
        template: __webpack_require__(/*! raw-loader!./response.component.html */ "./node_modules/raw-loader/index.js!./src/app/components/response/response.component.html"),
        styles: [__webpack_require__(/*! ./response.component.css */ "./src/app/components/response/response.component.css")]
    })
], ResponseComponent);



/***/ }),

/***/ "./src/app/components/rules/rules.component.css":
/*!******************************************************!*\
  !*** ./src/app/components/rules/rules.component.css ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".rules_header img{\n  height: 100px;\n  width: 100px;\n}\n\n.rules_header{\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-orient: horizontal;\n  -webkit-box-direction: normal;\n          flex-direction: row;\n  -webkit-box-pack: justify;\n          justify-content: space-between;\n  -webkit-box-align: center;\n          align-items: center;\n  padding: 0 10px;\n}\n\n.rules_header h1{\n  color: #311B92;\n}\n\n.ruleHeading{\n  font-family: 'Roboto', sans-serif;\n  font-size: 40px;\n  letter-spacing: 3px;\n  color: #212121;\n  margin-bottom: 20px;\n}\n\n.rulesSubHeading{\n  color:#9E9E9E;\n  margin: 30px 0;\n  font-size: 20px;\n}\n\n.rules{\n  text-align: center;\n  font-family: \"Trebuchet MS\", \"Lucida Grande\", \"Lucida Sans Unicode\", \"Lucida Sans\", Tahoma, sans-serif;\n  list-style-position: inside;\n}\n\n.submit_button{\n  text-align: center;\n  margin: 2rem;\n\n}\n\n.button{\n  background-color: #4CAF50;\n  border: none;\n  color: white;\n  padding: 15px 32px;\n  text-align: center;\n  text-decoration: none;\n  display: inline-block;\n  font-size: 16px;\n  cursor: pointer;\n  font-family: 'Poppins',sans-serif;\n  border-radius: 300px;\n}\n\n.check{\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-orient: horizontal;\n  -webkit-box-direction: normal;\n          flex-direction: row;\n  -webkit-box-pack: justify;\n          justify-content: space-between;\n  padding: 0 40% 0 40%;\n\n}\n\nol{\n  color: #1A237E;\n  font-size: 1.5rem;\n  font-family: 'Montserrat', sans-serif;\n  box-shadow: 0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23);\n  padding: 20px;\n  font-weight: 500;\n  width: 50%;\n  margin-left: 50%;\n  -webkit-transform: translateX(-50%);\n          transform: translateX(-50%);\n  border-bottom: 5px solid red;\n  margin-bottom: 2rem;\n}\n\nli{\n  margin: 1.5rem 0;\n  text-align: justify;\n}\n\nul {\n  margin-top: -4.2%;\n}\n\n.optionsSec{\n  margin-top: 2rem;\n}\n\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9ydWxlcy9ydWxlcy5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsYUFBYTtFQUNiLFlBQVk7QUFDZDs7QUFFQTtFQUNFLG9CQUFhO0VBQWIsYUFBYTtFQUNiLDhCQUFtQjtFQUFuQiw2QkFBbUI7VUFBbkIsbUJBQW1CO0VBQ25CLHlCQUE4QjtVQUE5Qiw4QkFBOEI7RUFDOUIseUJBQW1CO1VBQW5CLG1CQUFtQjtFQUNuQixlQUFlO0FBQ2pCOztBQUNBO0VBQ0UsY0FBYztBQUNoQjs7QUFDQTtFQUNFLGlDQUFpQztFQUNqQyxlQUFlO0VBQ2YsbUJBQW1CO0VBQ25CLGNBQWM7RUFDZCxtQkFBbUI7QUFDckI7O0FBQ0E7RUFDRSxhQUFhO0VBQ2IsY0FBYztFQUNkLGVBQWU7QUFDakI7O0FBQ0E7RUFDRSxrQkFBa0I7RUFDbEIsc0dBQXNHO0VBQ3RHLDJCQUEyQjtBQUM3Qjs7QUFDQTtFQUNFLGtCQUFrQjtFQUNsQixZQUFZOztBQUVkOztBQUVBO0VBQ0UseUJBQXlCO0VBQ3pCLFlBQVk7RUFDWixZQUFZO0VBQ1osa0JBQWtCO0VBQ2xCLGtCQUFrQjtFQUNsQixxQkFBcUI7RUFDckIscUJBQXFCO0VBQ3JCLGVBQWU7RUFDZixlQUFlO0VBQ2YsaUNBQWlDO0VBQ2pDLG9CQUFvQjtBQUN0Qjs7QUFFQTtFQUNFLG9CQUFhO0VBQWIsYUFBYTtFQUNiLDhCQUFtQjtFQUFuQiw2QkFBbUI7VUFBbkIsbUJBQW1CO0VBQ25CLHlCQUE4QjtVQUE5Qiw4QkFBOEI7RUFDOUIsb0JBQW9COztBQUV0Qjs7QUFFQTtFQUNFLGNBQWM7RUFDZCxpQkFBaUI7RUFDakIscUNBQXFDO0VBQ3JDLGtFQUFrRTtFQUNsRSxhQUFhO0VBQ2IsZ0JBQWdCO0VBQ2hCLFVBQVU7RUFDVixnQkFBZ0I7RUFDaEIsbUNBQTJCO1VBQTNCLDJCQUEyQjtFQUMzQiw0QkFBNEI7RUFDNUIsbUJBQW1CO0FBQ3JCOztBQUNBO0VBQ0UsZ0JBQWdCO0VBQ2hCLG1CQUFtQjtBQUNyQjs7QUFDQTtFQUNFLGlCQUFpQjtBQUNuQjs7QUFDQTtFQUNFLGdCQUFnQjtBQUNsQiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvcnVsZXMvcnVsZXMuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5ydWxlc19oZWFkZXIgaW1ne1xuICBoZWlnaHQ6IDEwMHB4O1xuICB3aWR0aDogMTAwcHg7XG59XG5cbi5ydWxlc19oZWFkZXJ7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgcGFkZGluZzogMCAxMHB4O1xufVxuLnJ1bGVzX2hlYWRlciBoMXtcbiAgY29sb3I6ICMzMTFCOTI7XG59XG4ucnVsZUhlYWRpbmd7XG4gIGZvbnQtZmFtaWx5OiAnUm9ib3RvJywgc2Fucy1zZXJpZjtcbiAgZm9udC1zaXplOiA0MHB4O1xuICBsZXR0ZXItc3BhY2luZzogM3B4O1xuICBjb2xvcjogIzIxMjEyMTtcbiAgbWFyZ2luLWJvdHRvbTogMjBweDtcbn1cbi5ydWxlc1N1YkhlYWRpbmd7XG4gIGNvbG9yOiM5RTlFOUU7XG4gIG1hcmdpbjogMzBweCAwO1xuICBmb250LXNpemU6IDIwcHg7XG59XG4ucnVsZXN7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgZm9udC1mYW1pbHk6IFwiVHJlYnVjaGV0IE1TXCIsIFwiTHVjaWRhIEdyYW5kZVwiLCBcIkx1Y2lkYSBTYW5zIFVuaWNvZGVcIiwgXCJMdWNpZGEgU2Fuc1wiLCBUYWhvbWEsIHNhbnMtc2VyaWY7XG4gIGxpc3Qtc3R5bGUtcG9zaXRpb246IGluc2lkZTtcbn1cbi5zdWJtaXRfYnV0dG9ue1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIG1hcmdpbjogMnJlbTtcblxufVxuXG4uYnV0dG9ue1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjNENBRjUwO1xuICBib3JkZXI6IG5vbmU7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgcGFkZGluZzogMTVweCAzMnB4O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICBmb250LXNpemU6IDE2cHg7XG4gIGN1cnNvcjogcG9pbnRlcjtcbiAgZm9udC1mYW1pbHk6ICdQb3BwaW5zJyxzYW5zLXNlcmlmO1xuICBib3JkZXItcmFkaXVzOiAzMDBweDtcbn1cblxuLmNoZWNre1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gIHBhZGRpbmc6IDAgNDAlIDAgNDAlO1xuXG59XG5cbm9se1xuICBjb2xvcjogIzFBMjM3RTtcbiAgZm9udC1zaXplOiAxLjVyZW07XG4gIGZvbnQtZmFtaWx5OiAnTW9udHNlcnJhdCcsIHNhbnMtc2VyaWY7XG4gIGJveC1zaGFkb3c6IDAgM3B4IDZweCByZ2JhKDAsMCwwLDAuMTYpLCAwIDNweCA2cHggcmdiYSgwLDAsMCwwLjIzKTtcbiAgcGFkZGluZzogMjBweDtcbiAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgd2lkdGg6IDUwJTtcbiAgbWFyZ2luLWxlZnQ6IDUwJTtcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGVYKC01MCUpO1xuICBib3JkZXItYm90dG9tOiA1cHggc29saWQgcmVkO1xuICBtYXJnaW4tYm90dG9tOiAycmVtO1xufVxubGl7XG4gIG1hcmdpbjogMS41cmVtIDA7XG4gIHRleHQtYWxpZ246IGp1c3RpZnk7XG59XG51bCB7XG4gIG1hcmdpbi10b3A6IC00LjIlO1xufVxuLm9wdGlvbnNTZWN7XG4gIG1hcmdpbi10b3A6IDJyZW07XG59XG5cbiJdfQ== */"

/***/ }),

/***/ "./src/app/components/rules/rules.component.ts":
/*!*****************************************************!*\
  !*** ./src/app/components/rules/rules.component.ts ***!
  \*****************************************************/
/*! exports provided: RulesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RulesComponent", function() { return RulesComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _services_test_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/test.service */ "./src/app/services/test.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");





let RulesComponent = class RulesComponent {
    constructor(testService, router) {
        this.testService = testService;
        this.router = router;
        this.controls = {
            type: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('c', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required])
        };
        this.form = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"](this.controls);
    }
    ngOnInit() {
    }
    startTest() {
        const data = this.form.value;
        console.log(data);
        this.router.navigate(['/test'], {
        // queryParams: {
        //   type: this.controls.type.value
        // }
        });
    }
};
RulesComponent.ctorParameters = () => [
    { type: _services_test_service__WEBPACK_IMPORTED_MODULE_3__["TestService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] }
];
RulesComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-rules',
        template: __webpack_require__(/*! raw-loader!./rules.component.html */ "./node_modules/raw-loader/index.js!./src/app/components/rules/rules.component.html"),
        styles: [__webpack_require__(/*! ./rules.component.css */ "./src/app/components/rules/rules.component.css")]
    })
], RulesComponent);



/***/ }),

/***/ "./src/app/components/test/test.component.css":
/*!****************************************************!*\
  !*** ./src/app/components/test/test.component.css ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "* {\n  margin: 0;\n  padding: 0;\n  font-family: 'Kanit', sans-serif;\n\n}\n\n.nav__container {\n  height: 15rem;\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n          flex-direction: column;\n  -webkit-box-pack: justify;\n          justify-content: space-between;\n}\n\n.container {\n  width: 100%;\n  display: grid;\n  height: 100vh;\n  grid-template-columns: 0.75fr repeat(9, 1fr);\n  grid-template-rows: repeat(10, 1fr);\n  grid-auto-flow: dense;\n}\n\n.heading {\n  margin: 5px 0;\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-align: center;\n          align-items: center;\n  -webkit-box-pack: center;\n          justify-content: center;\n  grid-column: 1/11;\n  grid-row: 1/2;\n\n}\n\n.heading h1 {\n  font-size: 3rem;\n  color: #1A237E;\n  padding: 0 1rem;\n}\n\n.heading img {\n  height: 70px;\n}\n\n.nav {\n  grid-gap: 0px;\n  background-color: #5F5F5F;\n  color: white;\n  font-family: 'Montserrat', sans-serif;\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-align: center;\n          align-items: center;\n  -webkit-box-pack: center;\n          justify-content: center;\n  cursor: pointer;\n  -webkit-box-flex: 1;\n          flex: 1;\n  font-size: 1.4rem;\n  font-family: 'Poppins', sans-serif;\n  font-weight: 500;\n  -webkit-transition: all .3s;\n  transition: all .3s;\n}\n\n.nav:hover {\n\n  color: white;\n  background-color: #4CAF50;\n}\n\n.nav:active {\n  background-color: #4CAF50;\n}\n\n.bgBlack {\n  background-color: #4CAF50;\n}\n\n.que {\n  margin-right: 5px;\n  grid-column: 2/9;\n  grid-row: 2/11;\n  overflow: auto;\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n          flex-direction: column;\n  padding: 0 2rem;\n}\n\n.nums {\n  /* background-color: salmon; */\n  grid-column: 9/11;\n  grid-row: 2/5;\n  padding: 0vh 5px 20px 5px;\n  display: grid;\n  grid-gap: 5px;\n  grid-template-columns: repeat(5, 1fr);\n  grid-template-rows: repeat(6, 1fr);\n}\n\n.nums .gd {\n  height: auto;\n  width: auto;\n  background-color: grey;\n  color: white;\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-align: center;\n          align-items: center;\n  -webkit-box-pack: center;\n          justify-content: center;\n}\n\n.instruct {\n  padding: 5px;\n  margin-bottom: 5px;\n  background-color: lightgray;\n  grid-column: 9/11;\n  /*grid-row: 6/8;*/\n  /*height: 5rem;*/\n  font-family: 'Poppins', sans-serif;\n}\n\n.instruct ul {\n  list-style: none;\n  width: 100%;\n  font-size: 1.2rem;\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n          flex-direction: column;\n}\n\n.instruct .grey {\n  display: inline-block;\n  height: 12px;\n  width: 25px;\n  background-color: grey;\n}\n\n.instruct .green {\n  display: inline-block;\n  height: 12px;\n  width: 25px;\n  background-color: #4CAF50;\n}\n\n.count_down {\n  font-size: 20px;\n  font-weight: 800;\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n          flex-direction: column;\n  -webkit-box-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n          align-items: center;\n  margin-bottom: 5px;\n  background-color: lightgray;\n  grid-column: 9/11;\n  /*grid-row: 8/9;*/\n  /*margin-top: -5rem;*/\n\n\n}\n\n#time {\n  width: 30rem;\n  text-align: center;\n  font-size: 50px;\n  font-weight: 900;\n  height: 10vh;\n  color: red;\n  border-radius: 10%;\n}\n\n.submit {\n  background-color: grey;\n  text-align: center;\n  font-size: 30px;\n  font-weight: 900;\n  color: white;\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-align: center;\n          align-items: center;\n  -webkit-box-pack: center;\n          justify-content: center;\n  grid-column: 9/11;\n  /*grid-row: 9/10;*/\n  cursor: pointer;\n}\n\n.submit:hover {\n\n  background-color: #4CAF50;\n}\n\n.questions {\n  margin: 1.5rem 0;\n  -webkit-transition: all 0.3s cubic-bezier(.25, .8, .25, 1);\n  transition: all 0.3s cubic-bezier(.25, .8, .25, 1);\n  /*padding: 1rem 1rem;*/\n  /*border-bottom: 1px solid grey;*/\n}\n\n.questions h3 {\n  margin-bottom: 1rem;\n}\n\n.types {\n  font-size: 25px;\n  font-weight: bold;\n  margin-top: 10px;\n  box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);\n  -webkit-transition: all 0.3s cubic-bezier(.25, .8, .25, 1);\n  transition: all 0.3s cubic-bezier(.25, .8, .25, 1);\n}\n\n.types:hover {\n  cursor: pointer;\n  box-shadow: 0 14px 28px rgba(0, 0, 0, 0.25), 0 10px 10px rgba(0, 0, 0, 0.22);\n}\n\n/*# sourceMappingURL=main.css.map */\n\n/*# sourceMappingURL=main.css.map */\n\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy90ZXN0L3Rlc3QuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLFNBQVM7RUFDVCxVQUFVO0VBQ1YsZ0NBQWdDOztBQUVsQzs7QUFFQTtFQUNFLGFBQWE7RUFDYixvQkFBYTtFQUFiLGFBQWE7RUFDYiw0QkFBc0I7RUFBdEIsNkJBQXNCO1VBQXRCLHNCQUFzQjtFQUN0Qix5QkFBOEI7VUFBOUIsOEJBQThCO0FBQ2hDOztBQUVBO0VBQ0UsV0FBVztFQUNYLGFBQWE7RUFDYixhQUFhO0VBQ2IsNENBQTRDO0VBQzVDLG1DQUFtQztFQUNuQyxxQkFBcUI7QUFDdkI7O0FBRUE7RUFDRSxhQUFhO0VBQ2Isb0JBQWE7RUFBYixhQUFhO0VBQ2IseUJBQW1CO1VBQW5CLG1CQUFtQjtFQUNuQix3QkFBdUI7VUFBdkIsdUJBQXVCO0VBQ3ZCLGlCQUFpQjtFQUNqQixhQUFhOztBQUVmOztBQUVBO0VBQ0UsZUFBZTtFQUNmLGNBQWM7RUFDZCxlQUFlO0FBQ2pCOztBQUVBO0VBQ0UsWUFBWTtBQUNkOztBQUdBO0VBQ0UsYUFBYTtFQUNiLHlCQUF5QjtFQUN6QixZQUFZO0VBQ1oscUNBQXFDO0VBQ3JDLG9CQUFhO0VBQWIsYUFBYTtFQUNiLHlCQUFtQjtVQUFuQixtQkFBbUI7RUFDbkIsd0JBQXVCO1VBQXZCLHVCQUF1QjtFQUN2QixlQUFlO0VBQ2YsbUJBQU87VUFBUCxPQUFPO0VBQ1AsaUJBQWlCO0VBQ2pCLGtDQUFrQztFQUNsQyxnQkFBZ0I7RUFDaEIsMkJBQW1CO0VBQW5CLG1CQUFtQjtBQUNyQjs7QUFFQTs7RUFFRSxZQUFZO0VBQ1oseUJBQXlCO0FBQzNCOztBQUVBO0VBQ0UseUJBQXlCO0FBQzNCOztBQUVBO0VBQ0UseUJBQXlCO0FBQzNCOztBQUVBO0VBQ0UsaUJBQWlCO0VBQ2pCLGdCQUFnQjtFQUNoQixjQUFjO0VBQ2QsY0FBYztFQUNkLG9CQUFhO0VBQWIsYUFBYTtFQUNiLDRCQUFzQjtFQUF0Qiw2QkFBc0I7VUFBdEIsc0JBQXNCO0VBQ3RCLGVBQWU7QUFDakI7O0FBRUE7RUFDRSw4QkFBOEI7RUFDOUIsaUJBQWlCO0VBQ2pCLGFBQWE7RUFDYix5QkFBeUI7RUFDekIsYUFBYTtFQUNiLGFBQWE7RUFDYixxQ0FBcUM7RUFDckMsa0NBQWtDO0FBQ3BDOztBQUVBO0VBQ0UsWUFBWTtFQUNaLFdBQVc7RUFDWCxzQkFBc0I7RUFDdEIsWUFBWTtFQUNaLG9CQUFhO0VBQWIsYUFBYTtFQUNiLHlCQUFtQjtVQUFuQixtQkFBbUI7RUFDbkIsd0JBQXVCO1VBQXZCLHVCQUF1QjtBQUN6Qjs7QUFFQTtFQUNFLFlBQVk7RUFDWixrQkFBa0I7RUFDbEIsMkJBQTJCO0VBQzNCLGlCQUFpQjtFQUNqQixpQkFBaUI7RUFDakIsZ0JBQWdCO0VBQ2hCLGtDQUFrQztBQUNwQzs7QUFFQTtFQUNFLGdCQUFnQjtFQUNoQixXQUFXO0VBQ1gsaUJBQWlCO0VBQ2pCLG9CQUFhO0VBQWIsYUFBYTtFQUNiLDRCQUFzQjtFQUF0Qiw2QkFBc0I7VUFBdEIsc0JBQXNCO0FBQ3hCOztBQUVBO0VBQ0UscUJBQXFCO0VBQ3JCLFlBQVk7RUFDWixXQUFXO0VBQ1gsc0JBQXNCO0FBQ3hCOztBQUVBO0VBQ0UscUJBQXFCO0VBQ3JCLFlBQVk7RUFDWixXQUFXO0VBQ1gseUJBQXlCO0FBQzNCOztBQUVBO0VBQ0UsZUFBZTtFQUNmLGdCQUFnQjtFQUNoQixvQkFBYTtFQUFiLGFBQWE7RUFDYiw0QkFBc0I7RUFBdEIsNkJBQXNCO1VBQXRCLHNCQUFzQjtFQUN0Qix3QkFBdUI7VUFBdkIsdUJBQXVCO0VBQ3ZCLHlCQUFtQjtVQUFuQixtQkFBbUI7RUFDbkIsa0JBQWtCO0VBQ2xCLDJCQUEyQjtFQUMzQixpQkFBaUI7RUFDakIsaUJBQWlCO0VBQ2pCLHFCQUFxQjs7O0FBR3ZCOztBQUVBO0VBQ0UsWUFBWTtFQUNaLGtCQUFrQjtFQUNsQixlQUFlO0VBQ2YsZ0JBQWdCO0VBQ2hCLFlBQVk7RUFDWixVQUFVO0VBQ1Ysa0JBQWtCO0FBQ3BCOztBQUVBO0VBQ0Usc0JBQXNCO0VBQ3RCLGtCQUFrQjtFQUNsQixlQUFlO0VBQ2YsZ0JBQWdCO0VBQ2hCLFlBQVk7RUFDWixvQkFBYTtFQUFiLGFBQWE7RUFDYix5QkFBbUI7VUFBbkIsbUJBQW1CO0VBQ25CLHdCQUF1QjtVQUF2Qix1QkFBdUI7RUFDdkIsaUJBQWlCO0VBQ2pCLGtCQUFrQjtFQUNsQixlQUFlO0FBQ2pCOztBQUVBOztFQUVFLHlCQUF5QjtBQUMzQjs7QUFFQTtFQUNFLGdCQUFnQjtFQUNoQiwwREFBa0Q7RUFBbEQsa0RBQWtEO0VBQ2xELHNCQUFzQjtFQUN0QixpQ0FBaUM7QUFDbkM7O0FBRUE7RUFDRSxtQkFBbUI7QUFDckI7O0FBR0E7RUFDRSxlQUFlO0VBQ2YsaUJBQWlCO0VBQ2pCLGdCQUFnQjtFQUNoQix3RUFBd0U7RUFDeEUsMERBQWtEO0VBQWxELGtEQUFrRDtBQUNwRDs7QUFFQTtFQUNFLGVBQWU7RUFDZiw0RUFBNEU7QUFDOUU7O0FBRUEsbUNBQW1DOztBQUNuQyxtQ0FBbUMiLCJmaWxlIjoic3JjL2FwcC9jb21wb25lbnRzL3Rlc3QvdGVzdC5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiKiB7XG4gIG1hcmdpbjogMDtcbiAgcGFkZGluZzogMDtcbiAgZm9udC1mYW1pbHk6ICdLYW5pdCcsIHNhbnMtc2VyaWY7XG5cbn1cblxuLm5hdl9fY29udGFpbmVyIHtcbiAgaGVpZ2h0OiAxNXJlbTtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xufVxuXG4uY29udGFpbmVyIHtcbiAgd2lkdGg6IDEwMCU7XG4gIGRpc3BsYXk6IGdyaWQ7XG4gIGhlaWdodDogMTAwdmg7XG4gIGdyaWQtdGVtcGxhdGUtY29sdW1uczogMC43NWZyIHJlcGVhdCg5LCAxZnIpO1xuICBncmlkLXRlbXBsYXRlLXJvd3M6IHJlcGVhdCgxMCwgMWZyKTtcbiAgZ3JpZC1hdXRvLWZsb3c6IGRlbnNlO1xufVxuXG4uaGVhZGluZyB7XG4gIG1hcmdpbjogNXB4IDA7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBncmlkLWNvbHVtbjogMS8xMTtcbiAgZ3JpZC1yb3c6IDEvMjtcblxufVxuXG4uaGVhZGluZyBoMSB7XG4gIGZvbnQtc2l6ZTogM3JlbTtcbiAgY29sb3I6ICMxQTIzN0U7XG4gIHBhZGRpbmc6IDAgMXJlbTtcbn1cblxuLmhlYWRpbmcgaW1nIHtcbiAgaGVpZ2h0OiA3MHB4O1xufVxuXG5cbi5uYXYge1xuICBncmlkLWdhcDogMHB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjNUY1RjVGO1xuICBjb2xvcjogd2hpdGU7XG4gIGZvbnQtZmFtaWx5OiAnTW9udHNlcnJhdCcsIHNhbnMtc2VyaWY7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBjdXJzb3I6IHBvaW50ZXI7XG4gIGZsZXg6IDE7XG4gIGZvbnQtc2l6ZTogMS40cmVtO1xuICBmb250LWZhbWlseTogJ1BvcHBpbnMnLCBzYW5zLXNlcmlmO1xuICBmb250LXdlaWdodDogNTAwO1xuICB0cmFuc2l0aW9uOiBhbGwgLjNzO1xufVxuXG4ubmF2OmhvdmVyIHtcblxuICBjb2xvcjogd2hpdGU7XG4gIGJhY2tncm91bmQtY29sb3I6ICM0Q0FGNTA7XG59XG5cbi5uYXY6YWN0aXZlIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzRDQUY1MDtcbn1cblxuLmJnQmxhY2sge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjNENBRjUwO1xufVxuXG4ucXVlIHtcbiAgbWFyZ2luLXJpZ2h0OiA1cHg7XG4gIGdyaWQtY29sdW1uOiAyLzk7XG4gIGdyaWQtcm93OiAyLzExO1xuICBvdmVyZmxvdzogYXV0bztcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgcGFkZGluZzogMCAycmVtO1xufVxuXG4ubnVtcyB7XG4gIC8qIGJhY2tncm91bmQtY29sb3I6IHNhbG1vbjsgKi9cbiAgZ3JpZC1jb2x1bW46IDkvMTE7XG4gIGdyaWQtcm93OiAyLzU7XG4gIHBhZGRpbmc6IDB2aCA1cHggMjBweCA1cHg7XG4gIGRpc3BsYXk6IGdyaWQ7XG4gIGdyaWQtZ2FwOiA1cHg7XG4gIGdyaWQtdGVtcGxhdGUtY29sdW1uczogcmVwZWF0KDUsIDFmcik7XG4gIGdyaWQtdGVtcGxhdGUtcm93czogcmVwZWF0KDYsIDFmcik7XG59XG5cbi5udW1zIC5nZCB7XG4gIGhlaWdodDogYXV0bztcbiAgd2lkdGg6IGF1dG87XG4gIGJhY2tncm91bmQtY29sb3I6IGdyZXk7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgZGlzcGxheTogZmxleDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG59XG5cbi5pbnN0cnVjdCB7XG4gIHBhZGRpbmc6IDVweDtcbiAgbWFyZ2luLWJvdHRvbTogNXB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiBsaWdodGdyYXk7XG4gIGdyaWQtY29sdW1uOiA5LzExO1xuICAvKmdyaWQtcm93OiA2Lzg7Ki9cbiAgLypoZWlnaHQ6IDVyZW07Ki9cbiAgZm9udC1mYW1pbHk6ICdQb3BwaW5zJywgc2Fucy1zZXJpZjtcbn1cblxuLmluc3RydWN0IHVsIHtcbiAgbGlzdC1zdHlsZTogbm9uZTtcbiAgd2lkdGg6IDEwMCU7XG4gIGZvbnQtc2l6ZTogMS4ycmVtO1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xufVxuXG4uaW5zdHJ1Y3QgLmdyZXkge1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gIGhlaWdodDogMTJweDtcbiAgd2lkdGg6IDI1cHg7XG4gIGJhY2tncm91bmQtY29sb3I6IGdyZXk7XG59XG5cbi5pbnN0cnVjdCAuZ3JlZW4ge1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gIGhlaWdodDogMTJweDtcbiAgd2lkdGg6IDI1cHg7XG4gIGJhY2tncm91bmQtY29sb3I6ICM0Q0FGNTA7XG59XG5cbi5jb3VudF9kb3duIHtcbiAgZm9udC1zaXplOiAyMHB4O1xuICBmb250LXdlaWdodDogODAwO1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgbWFyZ2luLWJvdHRvbTogNXB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiBsaWdodGdyYXk7XG4gIGdyaWQtY29sdW1uOiA5LzExO1xuICAvKmdyaWQtcm93OiA4Lzk7Ki9cbiAgLyptYXJnaW4tdG9wOiAtNXJlbTsqL1xuXG5cbn1cblxuI3RpbWUge1xuICB3aWR0aDogMzByZW07XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgZm9udC1zaXplOiA1MHB4O1xuICBmb250LXdlaWdodDogOTAwO1xuICBoZWlnaHQ6IDEwdmg7XG4gIGNvbG9yOiByZWQ7XG4gIGJvcmRlci1yYWRpdXM6IDEwJTtcbn1cblxuLnN1Ym1pdCB7XG4gIGJhY2tncm91bmQtY29sb3I6IGdyZXk7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgZm9udC1zaXplOiAzMHB4O1xuICBmb250LXdlaWdodDogOTAwO1xuICBjb2xvcjogd2hpdGU7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBncmlkLWNvbHVtbjogOS8xMTtcbiAgLypncmlkLXJvdzogOS8xMDsqL1xuICBjdXJzb3I6IHBvaW50ZXI7XG59XG5cbi5zdWJtaXQ6aG92ZXIge1xuXG4gIGJhY2tncm91bmQtY29sb3I6ICM0Q0FGNTA7XG59XG5cbi5xdWVzdGlvbnMge1xuICBtYXJnaW46IDEuNXJlbSAwO1xuICB0cmFuc2l0aW9uOiBhbGwgMC4zcyBjdWJpYy1iZXppZXIoLjI1LCAuOCwgLjI1LCAxKTtcbiAgLypwYWRkaW5nOiAxcmVtIDFyZW07Ki9cbiAgLypib3JkZXItYm90dG9tOiAxcHggc29saWQgZ3JleTsqL1xufVxuXG4ucXVlc3Rpb25zIGgzIHtcbiAgbWFyZ2luLWJvdHRvbTogMXJlbTtcbn1cblxuXG4udHlwZXMge1xuICBmb250LXNpemU6IDI1cHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBtYXJnaW4tdG9wOiAxMHB4O1xuICBib3gtc2hhZG93OiAwIDFweCAzcHggcmdiYSgwLCAwLCAwLCAwLjEyKSwgMCAxcHggMnB4IHJnYmEoMCwgMCwgMCwgMC4yNCk7XG4gIHRyYW5zaXRpb246IGFsbCAwLjNzIGN1YmljLWJlemllciguMjUsIC44LCAuMjUsIDEpO1xufVxuXG4udHlwZXM6aG92ZXIge1xuICBjdXJzb3I6IHBvaW50ZXI7XG4gIGJveC1zaGFkb3c6IDAgMTRweCAyOHB4IHJnYmEoMCwgMCwgMCwgMC4yNSksIDAgMTBweCAxMHB4IHJnYmEoMCwgMCwgMCwgMC4yMik7XG59XG5cbi8qIyBzb3VyY2VNYXBwaW5nVVJMPW1haW4uY3NzLm1hcCAqL1xuLyojIHNvdXJjZU1hcHBpbmdVUkw9bWFpbi5jc3MubWFwICovXG5cbiJdfQ== */"

/***/ }),

/***/ "./src/app/components/test/test.component.ts":
/*!***************************************************!*\
  !*** ./src/app/components/test/test.component.ts ***!
  \***************************************************/
/*! exports provided: TestComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TestComponent", function() { return TestComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _services_admin_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/admin.service */ "./src/app/services/admin.service.ts");
/* harmony import */ var _services_test_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/test.service */ "./src/app/services/test.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm2015/material.js");
/* harmony import */ var _services_user_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../services/user.service */ "./src/app/services/user.service.ts");
/* harmony import */ var _confirmation_dialog_confirmation_dialog_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../confirmation-dialog/confirmation-dialog.component */ "./src/app/components/confirmation-dialog/confirmation-dialog.component.ts");
/* harmony import */ var _form_can_deactivate_form_can_deactivate__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../form-can-deactivate/form-can-deactivate */ "./src/app/components/form-can-deactivate/form-can-deactivate.ts");










let TestComponent = class TestComponent extends _form_can_deactivate_form_can_deactivate__WEBPACK_IMPORTED_MODULE_9__["FormCanDeactivate"] {
    constructor(testService, adminService, userService, router, route, builder, matDialog) {
        super();
        this.testService = testService;
        this.adminService = adminService;
        this.userService = userService;
        this.router = router;
        this.route = route;
        this.builder = builder;
        this.matDialog = matDialog;
        // selectedType: string;
        this.types = ['html', 'css', 'js', 'nodejs'];
        // types: string[] = ['web', 'sql', 'aptitude'];
        this.demoTypes = [];
        this.questions = [];
        this.Arr = [false, false, false, false];
        this.num = 0;
        this.valid = true;
        this.responses = {};
        this.controls = {
            answers: this.builder.array([])
        };
        this.form = this.builder.group(this.controls);
    }
    getQuestionNumber(quesNumber) {
        this.num = ++quesNumber;
        console.log(this.num);
        return quesNumber;
    }
    ngOnInit() {
        this.valid = false;
        this.route.queryParamMap.subscribe((params) => {
            // this.selectedType = params.get('type');
            this.testService.startTest( /*{type: this.selectedType}*/)
                .subscribe(questions => {
                this.questions = questions;
                this.num = this.getQuestionNumber(this.num);
                this.questions.forEach(question => {
                    this.controls.answers.push(this.createAnswers(question.id));
                });
            });
        });
        this.demoTypes.push(/*this.selectedType, */ ...this.types);
        this.types = this.demoTypes;
    }
    createAnswers(questionId) {
        return this.builder.group({
            question_id: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](questionId, [_angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required]),
            option_id: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required])
        });
    }
    submitTest() {
        this.valid = true;
        const answers = [];
        Object.keys(this.responses).forEach(questionId => {
            answers.push({
                question_id: questionId,
                option_id: this.responses[questionId]
            });
        });
        this.matDialog.open(_confirmation_dialog_confirmation_dialog_component__WEBPACK_IMPORTED_MODULE_8__["ConfirmationDialogComponent"], {
            data: {
                answers
            }
        });
    }
    onQuestionResponse(questionId, optionId) {
        this.responses[questionId] = optionId;
        console.log('response', this.responses);
    }
    colorDiv(questionId) {
        console.log(questionId);
        document.getElementById('ques' + questionId).style.backgroundColor = '#4CAF50';
    }
    showHide(type, index) {
        for (let i = 0; i < 4; i++) {
            this.Arr[i] = false;
            document.getElementById(this.types[i]).style.display = 'none';
            document.getElementById(this.types[i]).style.height = '0';
        }
        this.Arr[index] = !this.Arr[index];
        document.getElementById(this.types[index]).style.display = 'block';
        document.getElementById(this.types[index]).style.height = 'auto';
    }
    get isValid() {
        return this.valid;
    }
};
TestComponent.ctorParameters = () => [
    { type: _services_test_service__WEBPACK_IMPORTED_MODULE_4__["TestService"] },
    { type: _services_admin_service__WEBPACK_IMPORTED_MODULE_3__["AdminService"] },
    { type: _services_user_service__WEBPACK_IMPORTED_MODULE_7__["UserService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormBuilder"] },
    { type: _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatDialog"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('countdown', null)
], TestComponent.prototype, "counter", void 0);
TestComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-test',
        template: __webpack_require__(/*! raw-loader!./test.component.html */ "./node_modules/raw-loader/index.js!./src/app/components/test/test.component.html"),
        styles: [__webpack_require__(/*! ./test.component.css */ "./src/app/components/test/test.component.css")]
    })
], TestComponent);



/***/ }),

/***/ "./src/app/services/admin.service.ts":
/*!*******************************************!*\
  !*** ./src/app/services/admin.service.ts ***!
  \*******************************************/
/*! exports provided: AdminService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminService", function() { return AdminService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _api_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./api.service */ "./src/app/services/api.service.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");




let AdminService = class AdminService {
    constructor(api) {
        this.api = api;
    }
    candidates() {
        return this.api.get('/candidates').pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(res => res.data));
    }
    questions() {
        return this.api.get('/questions').pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(res => res.data));
    }
    createCandidate(data) {
        return this.api.post('/candidates', data);
    }
    createQuestion(data) {
        return this.api.post('/questions', data);
    }
};
AdminService.ctorParameters = () => [
    { type: _api_service__WEBPACK_IMPORTED_MODULE_2__["ApiService"] }
];
AdminService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()
], AdminService);



/***/ }),

/***/ "./src/app/services/api.service.ts":
/*!*****************************************!*\
  !*** ./src/app/services/api.service.ts ***!
  \*****************************************/
/*! exports provided: ApiService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ApiService", function() { return ApiService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");

var ApiService_1;



let ApiService = ApiService_1 = class ApiService {
    constructor(http) {
        this.http = http;
        this.baseUrl = _environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].baseUrl;
    }
    static getToken() {
        return localStorage.getItem('auth_token');
    }
    get(url, data) {
        let headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]();
        headers = headers.set('Authorization', 'bearer ' + ApiService_1.getToken());
        return this.http.get(this.baseUrl + url, { params: data, headers });
    }
    post(url, data) {
        let headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]();
        headers = headers.set('Authorization', 'bearer ' + ApiService_1.getToken());
        return this.http.post(this.baseUrl + url, data, { headers });
    }
    put(url, data) {
        let headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]();
        headers = headers.set('Authorization', 'bearer ' + ApiService_1.getToken());
        return this.http.put(this.baseUrl + url, data, { headers });
    }
};
ApiService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
];
ApiService = ApiService_1 = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()
], ApiService);



/***/ }),

/***/ "./src/app/services/test.service.ts":
/*!******************************************!*\
  !*** ./src/app/services/test.service.ts ***!
  \******************************************/
/*! exports provided: TestService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TestService", function() { return TestService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _api_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./api.service */ "./src/app/services/api.service.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");




let TestService = class TestService {
    constructor(api) {
        this.api = api;
        this.testData = null;
    }
    // ${data.type}
    startTest( /*data: { type: string }*/) {
        return this.api.get(`/questions`).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(res => res.data), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])((val) => {
            this.testData = val;
        }));
    }
    createAnswers(data) {
        return this.api.post('/answers', data);
    }
    createScore() {
        return this.api.post('/result');
    }
    responses(data) {
        return this.api.post('/responses', data);
    }
};
TestService.ctorParameters = () => [
    { type: _api_service__WEBPACK_IMPORTED_MODULE_2__["ApiService"] }
];
TestService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()
], TestService);



/***/ }),

/***/ "./src/app/services/user.service.ts":
/*!******************************************!*\
  !*** ./src/app/services/user.service.ts ***!
  \******************************************/
/*! exports provided: UserService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserService", function() { return UserService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _api_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./api.service */ "./src/app/services/api.service.ts");



let UserService = class UserService {
    constructor(api) {
        this.api = api;
    }
    login(data) {
        return this.api.post('/authenticate', data);
    }
};
UserService.ctorParameters = () => [
    { type: _api_service__WEBPACK_IMPORTED_MODULE_2__["ApiService"] }
];
UserService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()
], UserService);



/***/ }),

/***/ "./src/environments/environment.prod.ts":
/*!**********************************************!*\
  !*** ./src/environments/environment.prod.ts ***!
  \**********************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
const environment = {
    production: true,
    baseUrl: "https://test-csi.herokuapp.com/api"
};


/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
const environment = {
    production: false,
    baseUrl: "https://test-csi.herokuapp.com/api",
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm2015/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment_prod__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment.prod */ "./src/environments/environment.prod.ts");
/*
  Author: Subhanshu Chaddha
  Contact: subhanshu.chaddha2@gmail.com
  College: AKGEC, Ghaziabad
*/




if (_environments_environment_prod__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(err => console.error(err));


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! D:\sites-new\test-frontend-master\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main-es2015.js.map