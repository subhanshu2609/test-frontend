/*
  Author: Subhanshu Chaddha
  Contact: subhanshu.chaddha2@gmail.com
  College: AKGEC, Ghaziabad
*/
export interface Answer {
  user_id: number;
  response: {question_id: number, option_id: number}[];
}
