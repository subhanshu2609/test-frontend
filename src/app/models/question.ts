/*
  Author: Subhanshu Chaddha
  Contact: subhanshu.chaddha2@gmail.com
  College: AKGEC, Ghaziabad
*/
import {Option} from './option';

export interface Question {
  id: number;
  type: string;
  question: string;
  options: {
    data: Option[];
  };
}
