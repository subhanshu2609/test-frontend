/*
  Author: Subhanshu Chaddha
  Contact: subhanshu.chaddha2@gmail.com
  College: AKGEC, Ghaziabad
*/
export interface User {
  id: number;
  name: string;
  role: string;
  student_number: number;
  email: string;
  phone_number: number;
  branch: string;
  gender: string;
  hosteler: boolean;
}
