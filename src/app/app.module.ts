/*
  Author: Subhanshu Chaddha
  Contact: subhanshu.chaddha2@gmail.com
  College: AKGEC, Ghaziabad
*/
import {BrowserModule} from '@angular/platform-browser';
import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {HttpClientModule} from '@angular/common/http';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {AdminComponent} from './components/admin/admin.component';
import {LoginComponent} from './components/login/login.component';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ApiService} from './services/api.service';
import {UserService} from './services/user.service';
import {RulesComponent} from './components/rules/rules.component';
import {TestComponent} from './components/test/test.component';
import {CreateCandidateComponent} from './components/create-candidate/create-candidate.component';
import {CreateQuestionComponent} from './components/create-question/create-question.component';
import {TestService} from './services/test.service';
import {CandidatesComponent} from './components/candidates/candidates.component';
import {AdminService} from './services/admin.service';
import {QuestionsComponent} from './components/questions/questions.component';
import {
  MatButtonModule,
  MatCardModule,
  MatDialogModule,
  MatFormFieldModule,
  MatInputModule,
  MatRadioModule,
  MatSelectModule,
  MatSnackBar, MatSnackBarModule
} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { CountdownTimerModule } from 'ngx-countdown-timer';
import {CountdownModule} from 'ngx-countdown';
import { ResponseComponent } from './components/response/response.component';
import { ConfirmationDialogComponent } from './components/confirmation-dialog/confirmation-dialog.component';
import {CanDeactivateGuard} from './components/can-deactivate/can-deactivate.guard';

@NgModule({
  declarations: [
    AppComponent,
    AdminComponent,
    LoginComponent,
    RulesComponent,
    TestComponent,
    CreateCandidateComponent,
    CreateQuestionComponent,
    CandidatesComponent,
    QuestionsComponent,
    ResponseComponent,
    ConfirmationDialogComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MatDialogModule,
    MatRadioModule,
    MatSelectModule,
    MatInputModule,
    MatSnackBarModule,
    MatFormFieldModule,
    MatCardModule,
    CountdownTimerModule,
    CountdownModule,
    MatButtonModule
  ],
  providers: [
    ApiService,
    UserService,
    TestService,
    AdminService,
    CanDeactivateGuard
  ],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule {
}
