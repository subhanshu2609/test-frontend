import {ComponentCanDeactivate} from '../can-deactivate/component-can-deactivate';

export abstract class FormCanDeactivate extends ComponentCanDeactivate {

  abstract get isValid(): boolean;

  canDeactivate(): boolean {
    return this.isValid ;
  }
}
