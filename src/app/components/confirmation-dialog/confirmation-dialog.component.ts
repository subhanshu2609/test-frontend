import {Component, Inject, OnInit} from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {TestService} from '../../services/test.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-confirmation-dialog',
  templateUrl: './confirmation-dialog.component.html',
  styleUrls: ['./confirmation-dialog.component.css']
})
export class ConfirmationDialogComponent implements OnInit {

  responses: { [questionId: number]: number } = {};

  constructor(private testService: TestService,
              private matDialog: MatDialog,
              private router: Router,
              private dialogRef: MatDialogRef<ConfirmationDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any) {
  }

  ngOnInit() {
    console.log(this.data);
  }

  yes() {


    console.log('answers', this.data);

    this.testService.createAnswers(this.data)
      .subscribe(res => {
        alert('Test Submitted Successfully');
        this.matDialog.closeAll();
        this.router.navigate(['/response']);
      }, error => {
        this.matDialog.closeAll();
        this.router.navigate(['/']);
        alert('Submission Failed');
      });
  }

  no() {
    this.dialogRef.close();
  }
}
