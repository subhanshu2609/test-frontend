import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Question} from '../../models/question';
import {AdminService} from '../../services/admin.service';
import {TestService} from '../../services/test.service';
import {FormBuilder, FormControl, FormGroup, NgForm, Validators} from '@angular/forms';
import {CountdownComponent} from 'ngx-countdown';
import {MatDialog} from '@angular/material';
import {UserService} from '../../services/user.service';
import {ConfirmationDialogComponent} from '../confirmation-dialog/confirmation-dialog.component';
import {FormCanDeactivate} from '../form-can-deactivate/form-can-deactivate';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css']
})

export class TestComponent extends FormCanDeactivate implements OnInit {

  @ViewChild('countdown', null) counter: CountdownComponent;

  constructor(private testService: TestService,
              private adminService: AdminService,
              private userService: UserService,
              private router: Router,
              private route: ActivatedRoute,
              private builder: FormBuilder,
              public matDialog: MatDialog) {
    super();
  }

  // selectedType: string;
  types: string[] = ['html', 'css', 'js', 'nodejs'];
  // types: string[] = ['web', 'sql', 'aptitude'];
  demoTypes: string[] = [];
  questions: Question[] = [];
  Arr = [false, false, false, false];
  num = 0;
  valid = true;

  responses: { [questionId: number]: number } = {};

  controls = {
    answers: this.builder.array([])
  };

  form = this.builder.group(this.controls);



  getQuestionNumber(quesNumber) {
    this.num = ++quesNumber;
    console.log(this.num);
    return quesNumber;
  }

  ngOnInit() {
    this.valid = false;
    this.route.queryParamMap.subscribe((params) => {
      // this.selectedType = params.get('type');
      this.testService.startTest(/*{type: this.selectedType}*/)
        .subscribe(questions => {
          this.questions = questions;
          this.num = this.getQuestionNumber(this.num);
          this.questions.forEach(question => {
            this.controls.answers.push(this.createAnswers(question.id));
          });
        });
    });
    this.demoTypes.push(/*this.selectedType, */...this.types);
    this.types = this.demoTypes;
  }

  createAnswers(questionId): FormGroup {
    return this.builder.group({
      question_id: new FormControl(questionId, [Validators.required]),
      option_id: new FormControl(null, [Validators.required])
    });
  }

  submitTest() {
    this.valid = true;
    const answers = [];
    Object.keys(this.responses).forEach(questionId => {
      answers.push({
        question_id: questionId,
        option_id: this.responses[questionId]
      });
    });
    this.matDialog.open(ConfirmationDialogComponent, {
      data: {
        answers
      }
    });
  }

  onQuestionResponse(questionId: number, optionId: number) {
    this.responses[questionId] = optionId;
    console.log('response', this.responses);
  }

  colorDiv(questionId) {
    console.log(questionId);
    document.getElementById('ques' + questionId).style.backgroundColor = '#4CAF50';
  }

  showHide(type, index) {
    for (let i = 0; i < 4; i++) {
      this.Arr[i] = false;
      document.getElementById(this.types[i]).style.display = 'none';
      document.getElementById(this.types[i]).style.height = '0';
    }
    this.Arr[index] = !this.Arr[index];
    document.getElementById(this.types[index]).style.display = 'block';
    document.getElementById(this.types[index]).style.height = 'auto';
  }
  get isValid(): boolean {
    return this.valid;
  }
}
