import {Component, OnInit} from '@angular/core';
import {User} from '../../models/user';
import {AdminService} from '../../services/admin.service';
import {MatDialog} from '@angular/material';
import {Router} from '@angular/router';
import {CreateCandidateComponent} from '../create-candidate/create-candidate.component';

@Component({
  selector: 'app-candidates',
  templateUrl: './candidates.component.html',
  styleUrls: ['./candidates.component.css']
})
export class CandidatesComponent implements OnInit {

  users: User[] = [];

  constructor(private adminService: AdminService,
              public dialog: MatDialog,
              public router: Router) {
  }

  ngOnInit() {
    this.adminService.candidates()
      .subscribe(res => {
        this.users = res;
      });
  }

  candidateForm() {
    const dialogref = this.dialog.open(CreateCandidateComponent);

    dialogref.afterClosed().subscribe(() => {
      this.adminService.candidates()
        .subscribe(res => {
          this.users = res;
        });
    });
  }


}
