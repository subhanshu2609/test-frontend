import {Component, OnInit} from '@angular/core';
import {Question} from '../../models/question';
import {AdminService} from '../../services/admin.service';
import {MatDialog} from '@angular/material';
import {CreateQuestionComponent} from '../create-question/create-question.component';

@Component({
  selector: 'app-questions',
  templateUrl: './questions.component.html',
  styleUrls: ['./questions.component.css']
})

export class QuestionsComponent implements OnInit {

  questions: Question[] = [];

  constructor(private adminService: AdminService,
              public dialog: MatDialog) {
  }

  ngOnInit() {
    this.adminService.questions()
      .subscribe(res => {
        this.questions = res;
      });
  }
  questionForm() {
    const dialogRef = this.dialog.open(CreateQuestionComponent);

    dialogRef.afterClosed().subscribe(() => {
      this.adminService.questions()
        .subscribe(res => {
          this.questions = res;
        });
    });
  }
}
