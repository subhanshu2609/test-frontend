import {Component, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {AdminService} from '../../services/admin.service';
import {MatDialogRef} from '@angular/material';

@Component({
  selector: 'app-create-question',
  templateUrl: './create-question.component.html',
  styleUrls: ['./create-question.component.css']
})
export class CreateQuestionComponent implements OnInit {

  controls = {
    type: new FormControl(null, [Validators.required]),
    question: new FormControl(null, [Validators.required]),
    options: this.builder.array([
      this.createOption(),
      this.createOption(),
      this.createOption(),
      this.createOption(),
    ])
  };
  form = this.builder.group(this.controls);

  constructor(private adminService: AdminService,
              private builder: FormBuilder,
              private dialogRef: MatDialogRef<CreateQuestionComponent>) {
  }

  ngOnInit() {
  }

  createOption(): FormGroup {
    return this.builder.group({
      title: new FormControl(null, [Validators.required]),
      is_correct: new FormControl(false, [Validators.required])
    });
  }

  get formData() { return this.form.get('options') as FormArray; }
  createQuestion() {
    if (this.form.invalid) {
      return;
    }
    const data = this.form.value;

    this.adminService.createQuestion(data)
      .subscribe(res => {
        this.dialogRef.close();
        // this.router.(['/candidate']);
      });
  }
}

