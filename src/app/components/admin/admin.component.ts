import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {AdminService} from '../../services/admin.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  constructor(private adminService: AdminService,
              private router: Router) {
  }

  ngOnInit() {
  }

  candidates() {
    this.adminService.candidates()
      .subscribe(res => {
        this.router.navigate(['/candidates']);
      });
  }

  questions() {
    this.router.navigate(['/questions']);
  }
}
