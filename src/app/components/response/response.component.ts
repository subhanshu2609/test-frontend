import { Component, OnInit } from '@angular/core';
import {MatDialogRef} from '@angular/material';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {TestService} from '../../services/test.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-response',
  templateUrl: './response.component.html',
  styleUrls: ['./response.component.css']
})
export class ResponseComponent implements OnInit {

  controls = {
    input: new FormControl(null, Validators.required)
  };

  form = new FormGroup((this.controls));
  loading: boolean;

  constructor(private testService: TestService,
              private router: Router) { }

  ngOnInit() {
    this.testService.createScore()
      .subscribe();
  }

  submitResponse() {
    const data = this.form.value;
    this.testService.responses(data)
      .subscribe(() => {
        this.router.navigate(['/']);
      });
  }

}
