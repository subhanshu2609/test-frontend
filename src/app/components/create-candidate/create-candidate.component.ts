import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AdminService} from '../../services/admin.service';
import {Router} from '@angular/router';
import {MatDialogRef} from '@angular/material';

@Component({
  selector: 'app-create-candidate',
  templateUrl: './create-candidate.component.html',
  styleUrls: ['./create-candidate.component.css']
})
export class CreateCandidateComponent implements OnInit {

  controls = {
    name: new FormControl(null, [Validators.required]),
    student_number: new FormControl(null, [Validators.required]),
    email: new FormControl(null, [Validators.email, Validators.required]),
    phone_number: new FormControl(null, [Validators.required]),
    branch: new FormControl(null, [Validators.required]),
    gender: new FormControl(null, [Validators.required]),
    hosteler: new FormControl(null, [Validators.required]),
  };

  form = new FormGroup(this.controls);
  loading: boolean;

  constructor(private adminService: AdminService,
              private dialogRef: MatDialogRef<CreateCandidateComponent>,
              private router: Router) {
  }

  ngOnInit() {
  }

  createCandidate() {

    if (this.form.invalid) {
      console.log('hii');
    }

    const data = this.form.value;

    this.adminService.createCandidate(data)
      .subscribe(res => {
        this.dialogRef.close();
        // this.router.(['/candidate']);
      });
  }

}
