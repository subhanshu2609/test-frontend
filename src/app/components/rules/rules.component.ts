import {Component, OnInit, HostListener} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {TestService} from '../../services/test.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-rules',
  templateUrl: './rules.component.html',
  styleUrls: ['./rules.component.css']
})
export class RulesComponent implements OnInit {

  constructor(private testService: TestService,
              private router: Router) {
  }

  controls = {
    type: new FormControl('c', [Validators.required])
  };

  form = new FormGroup(this.controls);
  loading: boolean;

  ngOnInit() {
  }

  startTest() {
    const data = this.form.value;
    console.log(data);

    this.router.navigate(['/test'], {
      // queryParams: {
      //   type: this.controls.type.value
      // }
    });
  }
}
