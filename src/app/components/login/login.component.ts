import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {UserService} from '../../services/user.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  errorMessage = 'Incorrect Login Details';
  controls = {
    email: new FormControl(null, [Validators.email, Validators.required]),
    password: new FormControl(null, [Validators.required])
  };

  form = new FormGroup(this.controls);
  loading: boolean;

  constructor(private userService: UserService,
              private router: Router) {
  }

  ngOnInit() {
  }

  login() {

    if (this.form.invalid) {
      return;
    }

    const data = this.form.value;

    this.userService.login(data)
      .subscribe(res => {
        localStorage.setItem('auth_token', res.token);
        if (res.user.role === 'admin') {
          this.router.navigate(['/admin-home']);
        } else {
          this.router.navigate(['/rules']);

        }
      }, error1 => {
        alert(this.errorMessage);
      });
  }

}
