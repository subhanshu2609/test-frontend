/*
  Author: Subhanshu Chaddha
  Contact: subhanshu.chaddha2@gmail.com
  College: AKGEC, Ghaziabad
*/
import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {LoginComponent} from './components/login/login.component';
import {AdminComponent} from './components/admin/admin.component';
import {RulesComponent} from './components/rules/rules.component';
import {TestComponent} from './components/test/test.component';
import {CreateCandidateComponent} from './components/create-candidate/create-candidate.component';
import {CreateQuestionComponent} from './components/create-question/create-question.component';
import {CandidatesComponent} from './components/candidates/candidates.component';
import {QuestionsComponent} from './components/questions/questions.component';
import {ResponseComponent} from './components/response/response.component';
import {ConfirmationDialogComponent} from './components/confirmation-dialog/confirmation-dialog.component';
import {CanDeactivateGuard} from './components/can-deactivate/can-deactivate.guard';


const routes: Routes = [
  {
    path: '',
    component: LoginComponent
  },
  {
    path: 'admin-home',
    component: AdminComponent
  },
  {
    path: 'rules',
    component: RulesComponent,
  },
  {
    path: 'test',
    component: TestComponent,
    canDeactivate: [CanDeactivateGuard]
  },
  {
    path: 'candidates',
    component: CandidatesComponent
  },
  {
    path: 'questions',
    component: QuestionsComponent
  },
  {
    path: 'createCandidate',
    component: CreateCandidateComponent
  },
  {
    path: 'createQuestion',
    component: CreateQuestionComponent
  },
  {
    path: 'response',
    component: ResponseComponent
  },
  {
    path: 'confirmation-dialog',
    component: ConfirmationDialogComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
