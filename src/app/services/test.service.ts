import {Injectable} from '@angular/core';
import {ApiService} from './api.service';
import {Observable} from 'rxjs';
import {map, tap} from 'rxjs/operators';

@Injectable()
export class TestService {

  constructor(private api: ApiService) {
  }

  testData: any = null;
  // ${data.type}
  startTest(/*data: { type: string }*/): Observable<any> {
    return this.api.get<any>(`/questions`).pipe(map(res => res.data), tap((val) => {
      this.testData = val;
    }));
  }

  createAnswers(data: { answers: { question_id: number, option_id: number }[] }): Observable<any> {
    return this.api.post('/answers', data);
  }

  createScore(): Observable<void> {
    return this.api.post('/result');
  }

  responses(data: {input: string}): Observable<Response> {
    return this.api.post('/responses', data);
  }
}
